<?php
// +----------------------------------------------------------------------
// | 药品管理 [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017 http://www.94park.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 晓月老板 < 1434926265@qq.com >
// +----------------------------------------------------------------------


namespace app\store\controller;

use \think\Cache;
use \think\Controller;
use think\Loader;
use think\Db;
use \think\Cookie;
use app\store\controller\User;
use app\store\model\Drug as DrugModel;
class Drug extends Controller
{

    /**
     * @name    药品展示
     * @author  1434926265@qq.com
     * @date    2018/01/11 14:09 PM
     * @return  array
     */
    public function index()
    {

        return $this->fetch();

    }

    /**
     * @todo    药品列表
     * @date    2018/01/11 14:09 PM
     * @author  1434926265@qq.com
     */
    public function lists()
    {

        $model = new DrugModel();

        $store_list_data = $model->paginate(20);

        foreach($store_list_data as $k => $v){

            if(isset($data['drug_store_house_id'])){

                $warehouse_name_data = Db::name('store_warehouse')->field('warehouse_name')->where(['status' => '1','id' => intval($v['drug_store_house_id'])])->find();

                $store_list_data[$k]['drug_store_house_id'] = isset($warehouse_name_data['warehouse_name'])?$warehouse_name_data['warehouse_name']:'';
            }

        }

        $this->assign('data',$store_list_data);

        return $this->fetch();

    }

    /**
     * @name    药品添加
     * @author  1434926265@qq.com
     * @date    2018/01/11 14:09 PM
     * @return  array
     */
    public function add()
    {

        // 判断post请求
        if($this->request->isPost()){

            $post_data = input('post.','','trim');

            // 格式化数据
            $insert_data = $this->format_drug_data($post_data);

            $model = new DrugModel($insert_data);

            // 过滤post数组中的非数据表字段数据 数据添加入库
            $result = $model->allowField(true)->save();

            if($result){

                return $this->success('添加成功','store/drug/lists');

            }else{

                return $this->success('添加失败','store/drug/add');

            }

        }

        // 获取 药品剂型
        $agent_data = Db::name('drug_agent_cate')->where('status',1)->select();
        $this->assign('agent_data',$agent_data);

        // 获取 药品性质
        $nature_data = Db::name('drug_nature_cate')->where('status',1)->select();
        $this->assign('nature_data',$nature_data);

        // 获取 药品目标市场
        $market_data = Db::name('drug_target_market_cate')->where('status',1)->select();
        $this->assign('market_data',$market_data);

        // 获取 药品用途
        $use_data = Db::name('drug_use_cate')->where('status',1)->select();
        $this->assign('use_data',$use_data);

        // 获取 药品用途
        $warehouse_data = Db::name('store_warehouse')->where('status',1)->select();
        $this->assign('warehouse_data',$warehouse_data);

        // 获取 药品规格
        $spec_data = Db::name('drug_spec')->where('status',1)->select();
        $this->assign('spec_data',$spec_data);

        return $this->fetch();

    }

    /**
     * @name    药品编辑
     * @author  1434926265@qq.com
     * @date    2018/01/11 14:09 PM
     * @return  boolean
     */
    public function edit()
    {

        //获取药品id
        $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;

        // 判断post请求
        if($this->request->isPost()){

            $post_data = input('post.','','trim');

            // 格式化数据
            $update_data = $this->format_drug_data($post_data);

            $model = new DrugModel();

            // 过滤post数组中的非数据表字段数据 数据添加入库
            $result = $model->allowField(true)->save($update_data, array('id'=>$id));

            if($result){

                return $this->success('修改成功','store/drug/lists');

            }else{

                return $this->success('修改失败');

            }

        }else{

            $model = new DrugModel();

            $data = $model->where('id',$id)->find()->toArray();

            // 获取药企id
            $this->assign('drug_store_id',Cookie::get('admin'));

            $this->assign('data',$data);

        }

        // 获取 药品剂型
        $agent_data = Db::name('drug_agent_cate')->where('status',1)->select();
        $this->assign('agent_data',$agent_data);

        // 获取 药品性质
        $nature_data = Db::name('drug_nature_cate')->where('status',1)->select();
        $this->assign('nature_data',$nature_data);

        // 获取 药品目标市场
        $market_data = Db::name('drug_target_market_cate')->where('status',1)->select();
        $this->assign('market_data',$market_data);

        // 获取 药品用途
        $use_data = Db::name('drug_use_cate')->where('status',1)->select();
        $this->assign('use_data',$use_data);

        // 获取 药品用途
        $warehouse_data = Db::name('store_warehouse')->where('status',1)->select();
        $this->assign('warehouse_data',$warehouse_data);

        // 获取 药品规格
        $spec_data = Db::name('drug_spec')->where('status',1)->select();
        $this->assign('spec_data',$spec_data);

        return $this->fetch();

    }

    /**
     * @name    药品删除
     * @author  1434926265@qq.com
     * @date    2018/01/11 14:09 PM
     * @return  integer
     */
    public function delete()
    {

        if($this->request->isAjax()) {

            $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;

            if(false == Db::name('drug')->where('id',$id)->delete()) {

                return $this->error('删除失败');

            } else {

                addlog($id);//写入日志

                return $this->success('删除成功','store/drug/lists');

            }
        }

    }


    /***
     * @name    格式化入库数据
     * @return  array
     */
    public function format_drug_data($data){

        if(is_array($data)){

            // 药品剂型
            if(isset($data['drug_agent_cate'])){
                $data['drug_agent_cate_id'] = isset(explode('|',$data['drug_agent_cate'])[0])?explode('|',$data['drug_agent_cate'])[0]:"";
                $data['drug_agent_cate_name'] = isset(explode('|',$data['drug_agent_cate'])[1])?explode('|',$data['drug_agent_cate'])[1]:"";
                unset($data['drug_agent_cate']);
            }

            // 药品性质
            if(isset($data['drug_nature_cate'])){
                $data['drug_nature_cate_id'] = isset(explode('|',$data['drug_nature_cate'])[0])?explode('|',$data['drug_nature_cate'])[0]:"";
                $data['drug_nature_cate_name'] = isset(explode('|',$data['drug_nature_cate'])[1])?explode('|',$data['drug_nature_cate'])[1]:"";
                unset($data['drug_nature_cate']);
            }

            // 药品目标市场
            if(isset($data['drug_market_cate'])){
                $data['drug_market_cate_id'] = isset(explode('|',$data['drug_market_cate'])[0])?explode('|',$data['drug_market_cate'])[0]:"";
                $data['drug_market_cate_name'] = isset(explode('|',$data['drug_market_cate'])[1])?explode('|',$data['drug_market_cate'])[1]:"";
                unset($data['drug_market_cate']);
            }

            // 药品用途
            if(isset($data['drug_use_cate'])){
                $data['drug_use_cate_id'] = isset(explode('|',$data['drug_use_cate'])[0])?explode('|',$data['drug_use_cate'])[0]:"";
                $data['drug_use_cate_name'] = isset(explode('|',$data['drug_use_cate'])[1])?explode('|',$data['drug_use_cate'])[1]:"";
                unset($data['drug_use_cate']);
            }

            // 药品仓库
            if(isset($data['drug_store_house'])){
                $data['drug_store_house_id'] = isset(explode('|',$data['drug_store_house'])[0])?explode('|',$data['drug_store_house'])[0]:"";
                $data['drug_store_house_name'] = isset(explode('|',$data['drug_store_house'])[1])?explode('|',$data['drug_store_house'])[1]:"";
                unset($data['drug_store_house']);
            }

            // 药品单位
            if(isset($data['drug_spec_data'])){
                $data['drug_spec_id'] = isset(explode('|',$data['drug_spec_data'])[0])?explode('|',$data['drug_spec_data'])[0]:"";
                $data['drug_spec_name'] = isset(explode('|',$data['drug_spec_data'])[1])?explode('|',$data['drug_spec_data'])[1]:"";
                unset($data['drug_spec_data']);
            }

            return $data;

        }else{

            return array();

        }

    }

}
