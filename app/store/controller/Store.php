<?php
// +----------------------------------------------------------------------
// | 药企管理 [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017 http://www.94park.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 晓月老板 < 1434926265@qq.com >
// +----------------------------------------------------------------------


namespace app\store\controller;

use \think\Cache;
use \think\Controller;
use think\Loader;
use think\Db;
use \think\Cookie;
use app\store\controller\User as User;
use app\store\model\Store as storeModel;
use app\store\model\Messages;
class Store extends Controller
{

    /**
     * @name    药企展示
     * @author  1434926265@qq.com
     * @date    2018/01/03 15:28 PM
     * @return  array
     */
    public function index()
    {

        return $this->fetch();

    }

    /**
     * @todo    药企列表
     * @date    20180104 13:18 PM
     * @author  1434926265@qq.com
     */
    public function lists()
    {

        $model = new storeModel();

        $store_list_data = $model->paginate(20);

        $this->assign('store',$store_list_data);

        return $this->fetch();

    }

    /**
     * @name    药企添加
     * @author  1434926265@qq.com
     * @date    2018/01/03 15:28 PM
     * @return  array
     */
    public function add()
    {

        // 判断post请求
        if($this->request->isPost()){

            $post_data = input('post.','','trim');

            $model = new storeModel($post_data);

            // 过滤post数组中的非数据表字段数据 数据添加入库
            $result = $model->allowField(true)->save();

            if($result){

                return $this->success('添加成功','admin/store/lists');

            }else{

                return $this->success('添加失败','admin/store/add');

            }

        }

        return $this->fetch();

    }

    /**
     * @name    药企编辑
     * @author  1434926265@qq.com
     * @date    2018/01/03 15:28 PM
     * @return  boolean
     */
    public function edit()
    {

        //获取药企id
        $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;

        // 判断post请求
        if($this->request->isPost()){

            $post_data = input('post.','','trim');

            $model = new storeModel();

            // 过滤post数组中的非数据表字段数据 数据添加入库
            $result = $model->allowField(true)->save($post_data, array('id'=>$id));

            if($result){

                return $this->success('修改成功','admin/store/lists');

            }else{

                return $this->success('修改失败');

            }

        }else{

            $model = new storeModel();

            $data = $model->where('id',$id)->find()->toArray();

            $this->assign('data',$data);

        }

        return $this->fetch();

    }

    /**
     * @name    药企删除
     * @author  1434926265@qq.com
     * @date    2018/01/03 15:28 PM
     * @return  integer
     */
    public function delete()
    {

        if($this->request->isAjax()) {

            $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;

            if(false == Db::name('store')->where('id',$id)->delete()) {

                return $this->error('删除失败');

            } else {

                addlog($id);//写入日志

                return $this->success('删除成功','admin/store/lists');

            }
        }

    }


}
