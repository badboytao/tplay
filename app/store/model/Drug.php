<?php
// +----------------------------------------------------------------------
// | Eyao [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017 http://www.94park.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 晓月老板 < 1434926265@qq.com >
// +----------------------------------------------------------------------


namespace app\store\model;

use \think\Model;
class Drug extends Model
{
    public function admin()
    {
        //关联管理员表
        return $this->belongsTo('Admin');
    }

}
