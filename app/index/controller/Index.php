<?php
// +----------------------------------------------------------------------
// | 前台模块 [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017 http://www.94park.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 晓月老板 < 1434926265@qq.com >
// +----------------------------------------------------------------------


namespace app\index\controller;

use \think\Cache;
use \think\Controller;
use think\Loader;
use think\Db;
use \think\Cookie;

class Index extends Controller
{

    public function index()
    {

        $this->assign('title','Fly 社区 - 基于 layui 的极简社区页面模版');

        return $this->fetch();

    }
}
