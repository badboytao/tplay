<?php
// +----------------------------------------------------------------------
// | 采购订单管理 [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017 http://www.94park.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 晓月老板 < 1434926265@qq.com >
// +----------------------------------------------------------------------


namespace app\pharmacy\controller;

use \think\Cache;
use \think\Controller;
use think\Loader;
use think\Db;
use \think\Cookie;
use app\pharmacy\controller\User;
use app\pharmacy\model\Order as OrderModel;
class Order extends Controller
{

    /**
     * @name    采购订单展示
     * @author  1434926265@qq.com
     * @date    2018/01/11 14:09 PM
     * @return  array
     */
    public function index()
    {

        return $this->fetch();

    }

    /**
     * @todo    采购订单列表
     * @date    2018/01/11 14:09 PM
     * @author  1434926265@qq.com
     */
    public function lists()
    {

        $model = new OrderModel();

        $pharmacy_list_data = $model->paginate(20);

        $this->assign('pharmacy',$pharmacy_list_data);

        return $this->fetch();

    }

    /**
     * @name    采购订单添加
     * @author  1434926265@qq.com
     * @date    2018/01/11 14:09 PM
     * @return  array
     */
    public function add()
    {

        // 判断post请求
        if($this->request->isPost()){

            $post_data = input('post.','','trim');

            $model = new OrderModel($post_data);

            // 过滤post数组中的非数据表字段数据 数据添加入库
            $result = $model->allowField(true)->save();

            if($result){

                return $this->success('添加成功','pharmacy/order/lists');

            }else{

                return $this->success('添加失败','pharmacy/order/add');

            }

        }

        return $this->fetch();

    }

    /**
     * @name    采购订单编辑
     * @author  1434926265@qq.com
     * @date    2018/01/11 14:09 PM
     * @return  boolean
     */
    public function edit()
    {

        //获取采购订单id
        $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;

        // 判断post请求
        if($this->request->isPost()){

            $post_data = input('post.','','trim');

            $model = new OrderModel();

            // 过滤post数组中的非数据表字段数据 数据添加入库
            $result = $model->allowField(true)->save($post_data, array('id'=>$id));

            if($result){

                return $this->success('修改成功','pharmacy/order/lists');

            }else{

                return $this->success('修改失败');

            }

        }else{

            $model = new OrderModel();

            $data = $model->where('id',$id)->find()->toArray();

            $this->assign('data',$data);

        }

        return $this->fetch();

    }

    /**
     * @name    采购订单删除
     * @author  1434926265@qq.com
     * @date    2018/01/11 14:09 PM
     * @return  integer
     */
    public function delete()
    {

        if($this->request->isAjax()) {

            $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;

            if(false == Db::name('pharmacy')->where('id',$id)->delete()) {

                return $this->error('删除失败');

            } else {

                addlog($id);//写入日志

                return $this->success('删除成功','pharmacy/order/lists');

            }
        }

    }


}
