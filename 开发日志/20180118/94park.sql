/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50553
Source Host           : 127.0.0.1:3306
Source Database       : 94park

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-01-18 18:04:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tplay_admin`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_admin`;
CREATE TABLE `tplay_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(20) DEFAULT NULL COMMENT '昵称',
  `name` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `thumb` int(11) NOT NULL DEFAULT '1' COMMENT '管理员头像',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '修改时间',
  `login_time` int(11) DEFAULT NULL COMMENT '最后登录时间',
  `login_ip` varchar(100) DEFAULT NULL COMMENT '最后登录ip',
  `admin_cate_id` int(2) NOT NULL DEFAULT '1' COMMENT '管理员分组',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tplay_admin
-- ----------------------------
INSERT INTO `tplay_admin` VALUES ('1', '晓月老板', 'admin', '31c64b511d1e90fcda8519941c1bd660', '6', '1510885948', '1515491693', '1516258021', '127.0.0.1', '1');
INSERT INTO `tplay_admin` VALUES ('2', '华润医药', 'store_001', '07ec58ba5aa0afbf37a489fd548fccb8', '3', '1515490862', '1515490862', '1516266239', '127.0.0.1', '2');
INSERT INTO `tplay_admin` VALUES ('3', '怡康医药', 'pharmacy_001', '0e2cccd7cde1ac8db82925fb7212872a', '4', '1515490918', '1515490918', '1516258046', '127.0.0.1', '3');

-- ----------------------------
-- Table structure for `tplay_admin_cate`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_admin_cate`;
CREATE TABLE `tplay_admin_cate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `permissions` text COMMENT '权限菜单',
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `desc` text COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tplay_admin_cate
-- ----------------------------
INSERT INTO `tplay_admin_cate` VALUES ('1', '超级管理员', '57,58,60,61,68,82,83,84,30,29,73,74,37,38,40,41,85,86,63,64,33,34,70,71,49,50,51,53,54,77,78,80', '1515490765', '1515044109', '超级管理员，拥有最高权限！');
INSERT INTO `tplay_admin_cate` VALUES ('2', '药企', '73,74,95,97,98,99,96', '1515490765', '1515650402', '药企管理');
INSERT INTO `tplay_admin_cate` VALUES ('3', '药店', '100,101,102,103,104', '1515490828', '1515651949', '药店管理');

-- ----------------------------
-- Table structure for `tplay_admin_log`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_admin_log`;
CREATE TABLE `tplay_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_menu_id` int(11) DEFAULT NULL COMMENT '操作菜单id',
  `admin_id` int(11) NOT NULL COMMENT '操作者id',
  `ip` varchar(100) DEFAULT NULL COMMENT '操作ip',
  `operation_id` varchar(200) DEFAULT NULL COMMENT '操作关联id',
  `create_time` int(11) NOT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=121 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tplay_admin_log
-- ----------------------------
INSERT INTO `tplay_admin_log` VALUES ('1', '66', '1', '127.0.0.1', '', '1514958668');
INSERT INTO `tplay_admin_log` VALUES ('2', '73', '1', '127.0.0.1', '1', '1514961796');
INSERT INTO `tplay_admin_log` VALUES ('3', '77', '1', '127.0.0.1', '85', '1514963476');
INSERT INTO `tplay_admin_log` VALUES ('4', '77', '1', '127.0.0.1', '85', '1514963617');
INSERT INTO `tplay_admin_log` VALUES ('5', '77', '1', '127.0.0.1', '86', '1514963808');
INSERT INTO `tplay_admin_log` VALUES ('6', '77', '1', '127.0.0.1', '86', '1514963833');
INSERT INTO `tplay_admin_log` VALUES ('7', '77', '1', '127.0.0.1', '1', '1514963853');
INSERT INTO `tplay_admin_log` VALUES ('8', '77', '1', '127.0.0.1', '86', '1514963925');
INSERT INTO `tplay_admin_log` VALUES ('9', '77', '1', '127.0.0.1', '87', '1514964135');
INSERT INTO `tplay_admin_log` VALUES ('10', '77', '1', '127.0.0.1', '88', '1514964215');
INSERT INTO `tplay_admin_log` VALUES ('11', '77', '1', '127.0.0.1', '88', '1514964264');
INSERT INTO `tplay_admin_log` VALUES ('12', '69', '1', '127.0.0.1', '2', '1514964313');
INSERT INTO `tplay_admin_log` VALUES ('13', '2', '1', '127.0.0.1', '1', '1514964314');
INSERT INTO `tplay_admin_log` VALUES ('14', '77', '1', '127.0.0.1', '88', '1514964628');
INSERT INTO `tplay_admin_log` VALUES ('15', '66', '1', '127.0.0.1', '', '1514966852');
INSERT INTO `tplay_admin_log` VALUES ('16', '77', '1', '127.0.0.1', '86', '1514967752');
INSERT INTO `tplay_admin_log` VALUES ('17', '77', '1', '127.0.0.1', '88', '1514967765');
INSERT INTO `tplay_admin_log` VALUES ('18', '77', '1', '127.0.0.1', '89', '1514968013');
INSERT INTO `tplay_admin_log` VALUES ('19', '77', '1', '127.0.0.1', '90', '1514968094');
INSERT INTO `tplay_admin_log` VALUES ('20', '77', '1', '127.0.0.1', '91', '1514968243');
INSERT INTO `tplay_admin_log` VALUES ('21', '77', '1', '127.0.0.1', '89', '1514968424');
INSERT INTO `tplay_admin_log` VALUES ('22', '66', '1', '127.0.0.1', '', '1515042806');
INSERT INTO `tplay_admin_log` VALUES ('23', '69', '1', '127.0.0.1', '3', '1515047495');
INSERT INTO `tplay_admin_log` VALUES ('24', '69', '1', '127.0.0.1', '4', '1515047640');
INSERT INTO `tplay_admin_log` VALUES ('25', '69', '1', '127.0.0.1', '5', '1515048053');
INSERT INTO `tplay_admin_log` VALUES ('26', '69', '1', '127.0.0.1', '6', '1515048670');
INSERT INTO `tplay_admin_log` VALUES ('27', '69', '1', '127.0.0.1', '7', '1515049155');
INSERT INTO `tplay_admin_log` VALUES ('28', '69', '1', '127.0.0.1', '8', '1515049689');
INSERT INTO `tplay_admin_log` VALUES ('29', '66', '1', '127.0.0.1', '', '1515050188');
INSERT INTO `tplay_admin_log` VALUES ('30', '69', '1', '127.0.0.1', '9', '1515051722');
INSERT INTO `tplay_admin_log` VALUES ('31', '91', '1', '127.0.0.1', '2', '1515051942');
INSERT INTO `tplay_admin_log` VALUES ('32', '66', '1', '127.0.0.1', '', '1515057534');
INSERT INTO `tplay_admin_log` VALUES ('33', '77', '1', '127.0.0.1', '92', '1515059118');
INSERT INTO `tplay_admin_log` VALUES ('34', '77', '1', '127.0.0.1', '93', '1515059175');
INSERT INTO `tplay_admin_log` VALUES ('35', '77', '1', '127.0.0.1', '94', '1515059237');
INSERT INTO `tplay_admin_log` VALUES ('36', '77', '1', '127.0.0.1', '92', '1515059266');
INSERT INTO `tplay_admin_log` VALUES ('37', '77', '1', '127.0.0.1', '88', '1515059290');
INSERT INTO `tplay_admin_log` VALUES ('38', '77', '1', '127.0.0.1', '93', '1515059300');
INSERT INTO `tplay_admin_log` VALUES ('39', '77', '1', '127.0.0.1', '94', '1515059310');
INSERT INTO `tplay_admin_log` VALUES ('40', '77', '1', '127.0.0.1', '88', '1515059469');
INSERT INTO `tplay_admin_log` VALUES ('41', '69', '1', '127.0.0.1', '10', '1515060246');
INSERT INTO `tplay_admin_log` VALUES ('42', '66', '1', '127.0.0.1', '', '1515082943');
INSERT INTO `tplay_admin_log` VALUES ('43', '66', '1', '127.0.0.1', '', '1515115149');
INSERT INTO `tplay_admin_log` VALUES ('44', '66', '1', '127.0.0.1', '', '1515135104');
INSERT INTO `tplay_admin_log` VALUES ('45', '66', '1', '127.0.0.1', '', '1515374626');
INSERT INTO `tplay_admin_log` VALUES ('46', '66', '1', '127.0.0.1', '', '1515396043');
INSERT INTO `tplay_admin_log` VALUES ('47', '66', '1', '127.0.0.1', '', '1515487562');
INSERT INTO `tplay_admin_log` VALUES ('48', '29', '1', '127.0.0.1', '19', '1515488086');
INSERT INTO `tplay_admin_log` VALUES ('49', '29', '1', '127.0.0.1', '20', '1515488122');
INSERT INTO `tplay_admin_log` VALUES ('50', '29', '1', '127.0.0.1', '20', '1515490765');
INSERT INTO `tplay_admin_log` VALUES ('51', '29', '1', '127.0.0.1', '3', '1515490828');
INSERT INTO `tplay_admin_log` VALUES ('52', '69', '1', '127.0.0.1', '3', '1515490844');
INSERT INTO `tplay_admin_log` VALUES ('53', '73', '1', '127.0.0.1', '16', '1515490862');
INSERT INTO `tplay_admin_log` VALUES ('54', '69', '1', '127.0.0.1', '4', '1515490902');
INSERT INTO `tplay_admin_log` VALUES ('55', '73', '1', '127.0.0.1', '3', '1515490918');
INSERT INTO `tplay_admin_log` VALUES ('56', '66', '2', '127.0.0.1', '', '1515490956');
INSERT INTO `tplay_admin_log` VALUES ('57', '69', '2', '127.0.0.1', '5', '1515491252');
INSERT INTO `tplay_admin_log` VALUES ('58', '66', '1', '127.0.0.1', '', '1515491488');
INSERT INTO `tplay_admin_log` VALUES ('59', '69', '1', '127.0.0.1', '6', '1515491612');
INSERT INTO `tplay_admin_log` VALUES ('60', '2', '1', '127.0.0.1', '1', '1515491613');
INSERT INTO `tplay_admin_log` VALUES ('61', '2', '1', '127.0.0.1', '1', '1515491693');
INSERT INTO `tplay_admin_log` VALUES ('62', '66', '1', '127.0.0.1', '', '1515554334');
INSERT INTO `tplay_admin_log` VALUES ('63', '66', '1', '127.0.0.1', '', '1515561976');
INSERT INTO `tplay_admin_log` VALUES ('64', '66', '1', '127.0.0.1', '', '1515562071');
INSERT INTO `tplay_admin_log` VALUES ('65', '66', '1', '127.0.0.1', '', '1515565152');
INSERT INTO `tplay_admin_log` VALUES ('66', '66', '1', '127.0.0.1', '', '1515566240');
INSERT INTO `tplay_admin_log` VALUES ('67', '66', '1', '127.0.0.1', '', '1515573884');
INSERT INTO `tplay_admin_log` VALUES ('68', '66', '1', '127.0.0.1', '', '1515634202');
INSERT INTO `tplay_admin_log` VALUES ('69', '77', '1', '127.0.0.1', '95', '1515635360');
INSERT INTO `tplay_admin_log` VALUES ('70', '77', '1', '127.0.0.1', '95', '1515635503');
INSERT INTO `tplay_admin_log` VALUES ('71', '66', '2', '127.0.0.1', '', '1515635578');
INSERT INTO `tplay_admin_log` VALUES ('72', '66', '1', '127.0.0.1', '', '1515635706');
INSERT INTO `tplay_admin_log` VALUES ('73', '77', '1', '127.0.0.1', '96', '1515635813');
INSERT INTO `tplay_admin_log` VALUES ('74', '77', '1', '127.0.0.1', '97', '1515635875');
INSERT INTO `tplay_admin_log` VALUES ('75', '77', '1', '127.0.0.1', '98', '1515636016');
INSERT INTO `tplay_admin_log` VALUES ('76', '77', '1', '127.0.0.1', '97', '1515636998');
INSERT INTO `tplay_admin_log` VALUES ('77', '66', '2', '127.0.0.1', '', '1515637952');
INSERT INTO `tplay_admin_log` VALUES ('78', '66', '2', '127.0.0.1', '', '1515638505');
INSERT INTO `tplay_admin_log` VALUES ('79', '66', '2', '127.0.0.1', '', '1515638739');
INSERT INTO `tplay_admin_log` VALUES ('80', '66', '2', '127.0.0.1', '', '1515638885');
INSERT INTO `tplay_admin_log` VALUES ('81', '66', '1', '127.0.0.1', '', '1515638919');
INSERT INTO `tplay_admin_log` VALUES ('82', '29', '1', '127.0.0.1', '2', '1515639537');
INSERT INTO `tplay_admin_log` VALUES ('83', '66', '2', '127.0.0.1', '', '1515639591');
INSERT INTO `tplay_admin_log` VALUES ('84', '66', '1', '127.0.0.1', '', '1515640378');
INSERT INTO `tplay_admin_log` VALUES ('85', '66', '2', '127.0.0.1', '', '1515647054');
INSERT INTO `tplay_admin_log` VALUES ('86', '66', '1', '127.0.0.1', '', '1515647632');
INSERT INTO `tplay_admin_log` VALUES ('87', '66', '1', '127.0.0.1', '', '1515648943');
INSERT INTO `tplay_admin_log` VALUES ('88', '77', '1', '127.0.0.1', '99', '1515649083');
INSERT INTO `tplay_admin_log` VALUES ('89', '77', '1', '127.0.0.1', '96', '1515649273');
INSERT INTO `tplay_admin_log` VALUES ('90', '77', '1', '127.0.0.1', '992', '1515649605');
INSERT INTO `tplay_admin_log` VALUES ('91', '77', '1', '127.0.0.1', '993', '1515649700');
INSERT INTO `tplay_admin_log` VALUES ('92', '77', '1', '127.0.0.1', '994', '1515649767');
INSERT INTO `tplay_admin_log` VALUES ('93', '77', '1', '127.0.0.1', '995', '1515649804');
INSERT INTO `tplay_admin_log` VALUES ('94', '77', '1', '127.0.0.1', '996', '1515649856');
INSERT INTO `tplay_admin_log` VALUES ('95', '29', '1', '127.0.0.1', '2', '1515650177');
INSERT INTO `tplay_admin_log` VALUES ('96', '29', '1', '127.0.0.1', '2', '1515650402');
INSERT INTO `tplay_admin_log` VALUES ('97', '66', '2', '127.0.0.1', '', '1515651662');
INSERT INTO `tplay_admin_log` VALUES ('98', '29', '1', '127.0.0.1', '3', '1515651949');
INSERT INTO `tplay_admin_log` VALUES ('99', '66', '1', '127.0.0.1', '', '1516094078');
INSERT INTO `tplay_admin_log` VALUES ('100', '66', '1', '127.0.0.1', '', '1516168196');
INSERT INTO `tplay_admin_log` VALUES ('101', '66', '1', '127.0.0.1', '', '1516176649');
INSERT INTO `tplay_admin_log` VALUES ('102', '66', '1', '127.0.0.1', '', '1516238239');
INSERT INTO `tplay_admin_log` VALUES ('103', '69', '1', '127.0.0.1', '21', '1516243325');
INSERT INTO `tplay_admin_log` VALUES ('104', '69', '1', '127.0.0.1', '33', '1516245136');
INSERT INTO `tplay_admin_log` VALUES ('105', '66', '1', '127.0.0.1', '', '1516246985');
INSERT INTO `tplay_admin_log` VALUES ('106', '77', '1', '127.0.0.1', '69', '1516252991');
INSERT INTO `tplay_admin_log` VALUES ('107', '77', '1', '127.0.0.1', '69', '1516253055');
INSERT INTO `tplay_admin_log` VALUES ('108', '77', '1', '127.0.0.1', '69', '1516253062');
INSERT INTO `tplay_admin_log` VALUES ('109', '66', '1', '127.0.0.1', '', '1516254204');
INSERT INTO `tplay_admin_log` VALUES ('110', '66', '1', '127.0.0.1', '', '1516255863');
INSERT INTO `tplay_admin_log` VALUES ('111', '66', '1', '127.0.0.1', '', '1516255931');
INSERT INTO `tplay_admin_log` VALUES ('112', '66', '1', '127.0.0.1', '', '1516256016');
INSERT INTO `tplay_admin_log` VALUES ('113', '66', '1', '127.0.0.1', '', '1516256255');
INSERT INTO `tplay_admin_log` VALUES ('114', '66', '1', '127.0.0.1', '', '1516256528');
INSERT INTO `tplay_admin_log` VALUES ('115', null, '2', '127.0.0.1', '', '1516257956');
INSERT INTO `tplay_admin_log` VALUES ('116', '66', '1', '127.0.0.1', '', '1516258021');
INSERT INTO `tplay_admin_log` VALUES ('117', null, '3', '127.0.0.1', '', '1516258046');
INSERT INTO `tplay_admin_log` VALUES ('118', null, '2', '127.0.0.1', '', '1516258229');
INSERT INTO `tplay_admin_log` VALUES ('119', null, '2', '127.0.0.1', '', '1516266239');
INSERT INTO `tplay_admin_log` VALUES ('120', null, '2', '127.0.0.1', '34', '1516268457');

-- ----------------------------
-- Table structure for `tplay_admin_menu`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_admin_menu`;
CREATE TABLE `tplay_admin_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `module` varchar(50) NOT NULL COMMENT '模块',
  `controller` varchar(100) NOT NULL COMMENT '控制器',
  `function` varchar(100) NOT NULL COMMENT '方法',
  `parameter` varchar(50) DEFAULT NULL COMMENT '参数',
  `description` varchar(250) DEFAULT NULL COMMENT '描述',
  `is_display` int(1) NOT NULL DEFAULT '1' COMMENT '1显示2隐藏',
  `type` int(1) NOT NULL DEFAULT '1' COMMENT '1:权限节点  2:普通节点',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '上级菜单0为顶级菜单',
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `icon` varchar(100) DEFAULT NULL COMMENT '图标',
  `is_open` int(1) NOT NULL DEFAULT '0' COMMENT '0默认闭合1默认展开',
  `orders` int(11) NOT NULL DEFAULT '0' COMMENT '排序值，越小越靠前',
  `auth` varchar(255) NOT NULL DEFAULT '1' COMMENT '权限归属[1:总管理后台 2:药企 3:药店]',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=105 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tplay_admin_menu
-- ----------------------------
INSERT INTO `tplay_admin_menu` VALUES ('1', '设置管理', 'admin', 'index', 'index', '', '管理软件的基础信息，包括个人的基本信息管理。', '1', '2', '0', '0', '1514963853', 'fa-cogs', '0', '1', '1|2|3');
INSERT INTO `tplay_admin_menu` VALUES ('2', '个人信息', 'admin', 'admin', 'personal', '', '对个人的一些信息进行管理。', '1', '2', '1', '0', '1513402673', 'fa-cog', '0', '1', '1|2|3');
INSERT INTO `tplay_admin_menu` VALUES ('4', '会员管理', 'admin', 'index', 'index', '', '后台管理员管理，包括后台权限组的管理。', '1', '2', '0', '1511015413', '1513558364', 'fa-user', '0', '2', '1|2|3');
INSERT INTO `tplay_admin_menu` VALUES ('6', '角色分组', 'admin', 'admin', 'adminCate', '', '管理员角色分组管理。', '1', '2', '4', '1511083098', '1513412856', 'fa-group', '0', '2', '1');
INSERT INTO `tplay_admin_menu` VALUES ('73', '添加/修改管理员', 'admin', 'admin', 'publish', '', '添加/修改管理员。', '2', '1', '72', '1513403009', '1513403009', '', '0', '0', '1|2|3');
INSERT INTO `tplay_admin_menu` VALUES ('74', '删除管理员', 'admin', 'admin', 'delete', '', '删除管理员。', '2', '1', '72', '1513403036', '1513403036', '', '0', '0', '1|2|3');
INSERT INTO `tplay_admin_menu` VALUES ('45', '日志管理', 'admin', 'index', 'index', '', '日志管理。', '1', '1', '0', '1511940197', '1513396527', 'fa-book', '0', '4', '1');
INSERT INTO `tplay_admin_menu` VALUES ('77', '添加/修改菜单', 'admin', 'menu', 'publish', '', '添加/修改菜单。', '2', '1', '76', '1513403367', '1513403367', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('78', '删除菜单', 'admin', 'menu', 'delete', '', '删除菜单。', '2', '1', '76', '1513403393', '1513403393', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('30', '删除权限分组', 'admin', 'admin', 'adminCateDelete', '', '删除后台管理员权限分组。', '2', '1', '6', '1511227568', '1513396473', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('13', '修改密码', 'admin', 'admin', 'editPassword', '', '修改个人登录密码。', '1', '2', '1', '1511083565', '1513395989', 'fa-edit', '0', '2', '1|2|3');
INSERT INTO `tplay_admin_menu` VALUES ('29', '添加/修改权限分组', 'admin', 'admin', 'adminCatePublish', '', '添加/修改管理员权限分组。', '2', '1', '6', '1511227503', '1513396481', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('79', '文件管理', 'admin', 'attachment', 'index', '', '文件管理。', '1', '1', '47', '1513403488', '1513404676', 'fa-file', '0', '1', '1');
INSERT INTO `tplay_admin_menu` VALUES ('33', '文件审核', 'admin', 'attachment', 'audit', '', '对文件进行审核。', '2', '1', '79', '1511227899', '1513403526', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('34', '文件删除', 'admin', 'attachment', 'delete', '', '对文件进行删除操作。', '2', '1', '79', '1511227936', '1513403541', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('35', '门户管理', 'admin', 'index', 'index', '', '门户内容管理', '1', '2', '0', '1511320705', '1513408714', 'fa-th', '0', '6', '1');
INSERT INTO `tplay_admin_menu` VALUES ('36', '分类管理', 'admin', 'articlecate', 'index', '', '分类列表管理。', '1', '1', '35', '1511320748', '1513402018', 'fa-tags', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('37', '添加/修改分类', 'admin', 'articlecate', 'publish', '', '添加/修改分类操作。', '2', '1', '36', '1511320794', '1513402031', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('38', '删除分类', 'admin', 'articlecate', 'delete', '', '删除分类操作。', '2', '1', '36', '1511320824', '1513402041', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('39', '文章管理', 'admin', 'article', 'index', '', '文章列表管理', '1', '1', '35', '1511320850', '1513402055', 'fa-file-text', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('40', '添加/修改文章', 'admin', 'article', 'publish', '', '添加/修改文章操作。', '2', '2', '39', '1511320883', '1513402066', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('41', '删除文章', 'admin', 'article', 'delete', '', '删除文章操作。', '2', '1', '39', '1511320907', '1513402079', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('46', '操作日志', 'admin', 'admin', 'log', '', '管理员操作日志。', '1', '1', '45', '1511940227', '1513396537', 'fa-book', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('47', '数据管理', 'admin', 'index', 'index', '', '数据相关的管理。', '1', '1', '0', '1511940263', '1513402145', 'fa-cubes', '0', '5', '1');
INSERT INTO `tplay_admin_menu` VALUES ('48', '数据库', 'admin', 'databackup', 'index', '', '数据库管理', '1', '1', '47', '1511940334', '1513402218', 'fa-database', '0', '2', '1');
INSERT INTO `tplay_admin_menu` VALUES ('49', '数据库备份', 'admin', 'databackup', 'export', '', '数据库备份。', '2', '1', '48', '1511940383', '1513402229', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('50', '数据库优化', 'admin', 'databackup', 'optimize', '', '数据库优化。', '2', '1', '48', '1511940422', '1513402239', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('51', '数据库修复', 'admin', 'databackup', 'repair', '', '数据库修复', '2', '1', '48', '1511940450', '1513402248', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('52', '备份管理', 'admin', 'databackup', 'importlist', '', '数据库备份文件管理。', '1', '1', '47', '1511940505', '1513402265', 'fa-bookmark', '0', '3', '1');
INSERT INTO `tplay_admin_menu` VALUES ('53', '数据库备份还原', 'admin', 'databackup', 'import', '', '数据库还原。', '2', '1', '52', '1511940554', '1513402275', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('54', '数据库备份删除', 'admin', 'databackup', 'del', '', '数据库备份删除。', '2', '1', '52', '1511940587', '1513402284', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('75', '菜单管理', 'admin', 'index', 'index', '', '菜单管理。', '1', '1', '0', '1513403151', '1513403151', 'fa-sitemap', '0', '3', '1');
INSERT INTO `tplay_admin_menu` VALUES ('56', '邮件配置', 'admin', 'emailconfig', 'index', '', '邮件配置。', '1', '1', '1', '1512811551', '1513402539', 'fa-envelope', '0', '4', '1');
INSERT INTO `tplay_admin_menu` VALUES ('57', '修改邮件配置', 'admin', 'emailconfig', 'publish', '', '修改邮件配置。', '2', '1', '56', '1512811595', '1513402369', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('58', '发送测试邮件', 'admin', 'emailconfig', 'mailto', '', '发送测试邮件。', '2', '1', '56', '1512811635', '1513402381', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('59', '短信配置', 'admin', 'smsconfig', 'index', '', '短信配置。', '1', '1', '1', '1512977784', '1513402562', 'fa-comment', '0', '5', '1');
INSERT INTO `tplay_admin_menu` VALUES ('60', '修改短信配置', 'admin', 'smsconfig', 'publish', '', '修改短信配置。', '2', '1', '59', '1512977821', '1513402412', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('61', '发送测试短信', 'admin', 'smsconfig', 'smsto', '', '发送测试短信。', '2', '1', '59', '1512977851', '1513402421', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('62', '留言管理', 'admin', 'tomessages', 'index', '', '留言管理。', '1', '2', '35', '1513047149', '1513402094', 'fa-comments', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('63', '标记留言', 'admin', 'tomessages', 'mark', '', '标记留言。', '2', '1', '62', '1513047177', '1513402105', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('64', '删除留言', 'admin', 'tomessages', 'delete', '', '删除留言。', '2', '1', '62', '1513047205', '1513402113', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('65', '添加留言', 'admin', 'tomessages', 'publish', '', '添加留言。', '2', '1', '62', '1513047239', '1513402120', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('66', '管理员登录', 'admin', 'common', 'login', '', '管理员登录。', '2', '2', '0', '1513061455', '1513402429', '', '0', '100', '1');
INSERT INTO `tplay_admin_menu` VALUES ('67', '网站配置', 'admin', 'webconfig', 'index', '', '网站信息设置。', '1', '1', '1', '1513131135', '1513408841', 'fa-desktop', '0', '3', '1');
INSERT INTO `tplay_admin_menu` VALUES ('68', '修改网站配置', 'admin', 'webconfig', 'publish', '', '修改网站配置信息。', '2', '1', '67', '1513131161', '1513408856', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('69', '上传文件', 'admin', 'common', 'upload', '', '上传文件。', '2', '1', '0', '1513155130', '1516253062', '', '0', '200', '1|2|3');
INSERT INTO `tplay_admin_menu` VALUES ('70', '上传附件', 'admin', 'attachment', 'upload', '', '上传附件。', '2', '1', '79', '1513323699', '1513403557', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('71', '文件下载', 'admin', 'attachment', 'download', '', '文件下载。', '2', '1', '79', '1513325699', '1513403571', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('72', '管理员', 'admin', 'admin', 'index', '', '管理员列表。', '1', '2', '4', '1513402959', '1513402959', 'fa-user', '0', '1', '1|2|3');
INSERT INTO `tplay_admin_menu` VALUES ('76', '后台菜单', 'admin', 'menu', 'index', '', '添加/修改菜单。', '1', '1', '75', '1513403248', '1513403248', 'fa-sliders', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('80', '菜单排序', 'admin', 'menu', 'orders', '', '后台菜单排序。', '2', '1', '76', '1513408418', '1513408418', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('81', 'URL美化', 'admin', 'urlsconfig', 'index', '', 'URL美化设置。', '1', '1', '1', '1513574783', '1513574783', 'fa-link', '0', '6', '1');
INSERT INTO `tplay_admin_menu` VALUES ('82', '新增/修改url美化', 'admin', 'urlsconfig', 'publish', '', '新增/修改url美化规则。', '2', '1', '81', '1513574935', '1513574935', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('83', '启用/禁用url美化', 'admin', 'urlsconfig', 'status', '', '启用/禁用url美化规则。', '2', '1', '81', '1513574979', '1513575215', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('84', '删除url美化规则', 'admin', 'urlsconfig', 'delete', '', '删除url美化规则。', '2', '1', '81', '1513575009', '1513575009', '', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('85', '药企管理', 'admin', 'store', 'index', '', '针对医药供货商的管理。', '1', '1', '0', '1514963476', '1514963617', 'fa-ambulance', '1', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('86', '供货企业列表', 'admin', 'store', 'lists', '', '医药供货商信息列表。', '1', '1', '85', '1514963808', '1514967752', 'fa-tasks', '1', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('87', '药店管理', 'admin', 'pharmacy', 'index', '', '入驻药店管理。', '1', '1', '0', '1514964135', '1514964135', 'fa-medkit', '1', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('88', '入驻药店列表', 'admin', 'pharmacy', 'lists', '', '入驻药店信息列表。', '1', '1', '87', '1514964215', '1515059469', 'fa-user-md', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('89', '供货企业添加', 'admin', 'store', 'add', '', '医药供货企业添加。', '1', '1', '85', '1514968013', '1514968424', 'fa-edit', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('90', '供货企业编辑', 'admin', 'store', 'edit', '', '医药供货企业编辑。', '2', '1', '85', '1514968094', '1514968094', 'fa-edit', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('91', '供货企业删除', 'admin', 'store', 'delete', '', '医药供货企业删除。', '2', '1', '85', '1514968243', '1514968243', 'fa-trash', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('92', '入驻药店添加', 'admin', 'pharmacy', 'add', '', '入驻药店添加。', '1', '1', '87', '1515059118', '1515059266', 'fa-edit', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('93', '入驻药店编辑', 'admin', 'pharmacy', 'edit', '', '入驻药店编辑。', '2', '1', '87', '1515059175', '1515059300', 'fa-edit', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('94', '入驻药店删除', 'admin', 'pharmacy', 'delete', '', '入驻药店删除。', '2', '1', '87', '1515059237', '1515059310', 'fa-trash', '0', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('95', '药品管理', 'store', 'index', 'index', '', '药企药品相关管理。', '1', '1', '0', '1515635360', '1515635503', 'fa-calendar', '0', '0', '2');
INSERT INTO `tplay_admin_menu` VALUES ('97', '药品添加', 'store', 'drug', 'add', '', '药企药品添加功能相关。', '1', '1', '95', '1515635813', '1515635813', 'fa-edit', '0', '0', '2');
INSERT INTO `tplay_admin_menu` VALUES ('98', '药品编辑', 'store', 'drug', 'edit', '', '药企药品编辑功能相关。', '2', '1', '95', '1515635875', '1515636998', 'fa-edit', '0', '0', '2');
INSERT INTO `tplay_admin_menu` VALUES ('99', '药品删除', 'store', 'drug', 'delete', '', '药企药品删除功能相关。', '2', '1', '95', '1515636016', '1515636016', 'fa-trash', '0', '0', '2');
INSERT INTO `tplay_admin_menu` VALUES ('96', '药品列表', 'store', 'drug', 'lists', '', '药企药品信息列表。', '1', '1', '95', '1515649083', '1515649273', 'fa-user-md', '0', '0', '2');
INSERT INTO `tplay_admin_menu` VALUES ('100', '采购管理', 'pharmacy', 'index', 'index', '', '订单采购管理。', '1', '1', '0', '1515649605', '1515649605', 'fa-ambulance', '0', '0', '3');
INSERT INTO `tplay_admin_menu` VALUES ('101', '采购订单列表', 'pharmacy', 'order', 'lists', '', '订单采购信息列表。', '1', '1', '100', '1515649700', '1515649700', 'fa-tasks', '0', '0', '3');
INSERT INTO `tplay_admin_menu` VALUES ('102', '采购订单', 'pharmacy', 'order', 'add', '', '订单采购添加。', '1', '1', '100', '1515649767', '1515649767', 'fa-edit', '0', '0', '3');
INSERT INTO `tplay_admin_menu` VALUES ('103', '采购订单编辑', 'pharmacy', 'order', 'edit', '', '订单采购编辑。', '2', '1', '100', '1515649804', '1515649804', 'fa-edit', '0', '0', '3');
INSERT INTO `tplay_admin_menu` VALUES ('104', '采购订单删除', 'pharmacy', 'order', 'delete', '', '采购订单删除。', '2', '1', '100', '1515649856', '1515649856', 'fa-trash', '0', '0', '3');

-- ----------------------------
-- Table structure for `tplay_article`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_article`;
CREATE TABLE `tplay_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `article_cate_id` int(11) NOT NULL,
  `thumb` int(11) DEFAULT NULL,
  `content` text,
  `admin_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `edit_admin_id` int(11) NOT NULL COMMENT '最后修改人',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0待审核1已审核',
  `is_top` int(1) NOT NULL DEFAULT '0' COMMENT '1置顶0普通',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `is_top` (`is_top`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tplay_article
-- ----------------------------

-- ----------------------------
-- Table structure for `tplay_article_cate`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_article_cate`;
CREATE TABLE `tplay_article_cate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `tag` varchar(250) DEFAULT NULL COMMENT '关键词',
  `description` varchar(250) DEFAULT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `pid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tplay_article_cate
-- ----------------------------

-- ----------------------------
-- Table structure for `tplay_attachment`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_attachment`;
CREATE TABLE `tplay_attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module` char(15) NOT NULL DEFAULT '' COMMENT '所属模块',
  `filename` char(50) NOT NULL DEFAULT '' COMMENT '文件名',
  `filepath` char(200) NOT NULL DEFAULT '' COMMENT '文件路径+文件名',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `fileext` char(10) NOT NULL DEFAULT '' COMMENT '文件后缀',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `uploadip` char(15) NOT NULL DEFAULT '' COMMENT '上传IP',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0未审核1已审核-1不通过',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  `admin_id` int(11) NOT NULL COMMENT '审核者id',
  `audit_time` int(11) NOT NULL COMMENT '审核时间',
  `use` varchar(200) DEFAULT NULL COMMENT '用处',
  `download` int(11) NOT NULL DEFAULT '0' COMMENT '下载量',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE,
  KEY `status` (`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='附件表';

-- ----------------------------
-- Records of tplay_attachment
-- ----------------------------
INSERT INTO `tplay_attachment` VALUES ('1', 'admin', '79811855a6c06de53047471c4ff82a36.jpg', '\\uploads\\admin\\admin_thumb\\20180104\\79811855a6c06de53047471c4ff82a36.jpg', '13781', 'jpg', '1', '127.0.0.1', '1', '1515046060', '1', '1515046060', 'admin_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('2', 'admin', 'fe6503be604640a0ad805a8158e3c77d.jpg', '\\uploads\\admin\\article_thumb\\20180104\\fe6503be604640a0ad805a8158e3c77d.jpg', '13781', 'jpg', '1', '127.0.0.1', '1', '1515055763', '1', '1515055763', 'article_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('3', 'admin', 'cec7b5179a31c88c1f910902e7aa4b0a.jpg', '\\uploads\\admin\\admin_thumb\\20180109\\cec7b5179a31c88c1f910902e7aa4b0a.jpg', '39098', 'jpg', '1', '127.0.0.1', '1', '1515490844', '1', '1515490844', 'admin_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('4', 'admin', 'b41262c1e7f93eaae6cdfb41fc7164f1.jpg', '\\uploads\\admin\\admin_thumb\\20180109\\b41262c1e7f93eaae6cdfb41fc7164f1.jpg', '39098', 'jpg', '1', '127.0.0.1', '1', '1515490902', '1', '1515490902', 'admin_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('5', 'admin', '99da37542e7c94f640df58bff513512f.jpg', '\\uploads\\admin\\pharmacy_licence_picture\\20180109\\99da37542e7c94f640df58bff513512f.jpg', '39098', 'jpg', '2', '127.0.0.1', '1', '1515491252', '2', '1515491252', 'pharmacy_licence_picture', '0');
INSERT INTO `tplay_attachment` VALUES ('6', 'admin', '77fd26f274aa2136dcd134a32c17a7da.jpg', '\\uploads\\admin\\admin_thumb\\20180109\\77fd26f274aa2136dcd134a32c17a7da.jpg', '39098', 'jpg', '1', '127.0.0.1', '1', '1515491612', '1', '1515491612', 'admin_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('7', 'admin', '5fc7bfd170c4b0079c5db9d630bb9add.jpg', '\\uploads\\admin\\store_licence_picture\\20180117\\5fc7bfd170c4b0079c5db9d630bb9add.jpg', '5788', 'jpg', '1', '127.0.0.1', '1', '1516180535', '1', '1516180535', 'store_licence_picture', '0');
INSERT INTO `tplay_attachment` VALUES ('8', 'admin', '4a97f9ca545a848dfcca2ce652da07e1.jpg', '\\uploads\\admin\\drug_thumb\\20180117\\4a97f9ca545a848dfcca2ce652da07e1.jpg', '39098', 'jpg', '1', '127.0.0.1', '1', '1516180814', '1', '1516180814', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('9', 'admin', '8013c1f85a8170d367505d6368f378f3.jpg', '\\uploads\\admin\\drug_thumb\\20180117\\8013c1f85a8170d367505d6368f378f3.jpg', '5788', 'jpg', '1', '127.0.0.1', '1', '1516180876', '1', '1516180876', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('10', 'admin', 'f9ddcd2ec602ea7f5b81fe065971ebc8.jpg', '\\uploads\\admin\\drug_thumb\\20180117\\f9ddcd2ec602ea7f5b81fe065971ebc8.jpg', '5788', 'jpg', '1', '127.0.0.1', '1', '1516180895', '1', '1516180895', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('11', 'admin', 'feee18fda6c31cc06df56e5e4ffd70bd.jpg', '\\uploads\\admin\\drug_thumb\\20180117\\feee18fda6c31cc06df56e5e4ffd70bd.jpg', '39098', 'jpg', '1', '127.0.0.1', '1', '1516180918', '1', '1516180918', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('12', 'admin', 'e73a6fb188efd92233f68087565b761b.jpg', '\\uploads\\admin\\drug_thumb\\20180117\\e73a6fb188efd92233f68087565b761b.jpg', '39098', 'jpg', '1', '127.0.0.1', '1', '1516181031', '1', '1516181031', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('13', 'admin', 'f367668eb51a7c74ec4f8d3055bf9e3c.jpg', '\\uploads\\admin\\drug_thumb\\20180117\\f367668eb51a7c74ec4f8d3055bf9e3c.jpg', '5788', 'jpg', '1', '127.0.0.1', '1', '1516181046', '1', '1516181046', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('14', 'admin', 'fef12cfa5fb0cbbd150bc604f7561030.jpg', '\\uploads\\admin\\drug_thumb\\20180117\\fef12cfa5fb0cbbd150bc604f7561030.jpg', '39098', 'jpg', '1', '127.0.0.1', '1', '1516181214', '1', '1516181214', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('15', 'admin', 'e717b0648366ed8db59bf3074e584d30.jpg', '\\uploads\\admin\\drug_thumb\\20180117\\e717b0648366ed8db59bf3074e584d30.jpg', '39098', 'jpg', '1', '127.0.0.1', '1', '1516181217', '1', '1516181217', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('16', 'admin', 'eb5b3b9580a7c8597c15298f1fa4cda1.jpg', '\\uploads\\admin\\drug_thumb\\20180118\\eb5b3b9580a7c8597c15298f1fa4cda1.jpg', '39098', 'jpg', '1', '127.0.0.1', '1', '1516238605', '1', '1516238605', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('17', 'admin', 'aea05299cee169d6d803c0f98946d46e.jpg', '\\uploads\\admin\\drug_thumb\\20180118\\aea05299cee169d6d803c0f98946d46e.jpg', '39098', 'jpg', '1', '127.0.0.1', '1', '1516242994', '1', '1516242994', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('18', 'admin', '79d8447f74b88c8ea1496e299342619b.jpg', '\\uploads\\admin\\drug_thumb\\20180118\\79d8447f74b88c8ea1496e299342619b.jpg', '5788', 'jpg', '1', '127.0.0.1', '1', '1516243000', '1', '1516243000', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('19', 'admin', 'a85d5fca97c4437e27199a50b92d53ea.jpg', '\\uploads\\admin\\drug_thumb\\20180118\\a85d5fca97c4437e27199a50b92d53ea.jpg', '39098', 'jpg', '1', '127.0.0.1', '1', '1516243025', '1', '1516243025', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('20', 'admin', '0b59509f87609d1b7775e915829638a6.jpg', '\\uploads\\admin\\drug_thumb\\20180118\\0b59509f87609d1b7775e915829638a6.jpg', '39098', 'jpg', '1', '127.0.0.1', '1', '1516243302', '1', '1516243302', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('21', 'admin', '71865a336b87a1dff29b0d67945c2a17.jpg', '\\uploads\\admin\\store_licence_picture\\20180118\\71865a336b87a1dff29b0d67945c2a17.jpg', '39098', 'jpg', '1', '127.0.0.1', '1', '1516243325', '1', '1516243325', 'store_licence_picture', '0');
INSERT INTO `tplay_attachment` VALUES ('22', 'admin', '1e34bfe75acd3eb7c61d850696f67c12.jpg', '\\uploads\\store\\drug_thumb\\20180118\\1e34bfe75acd3eb7c61d850696f67c12.jpg', '39098', 'jpg', '1', '127.0.0.1', '1', '1516243571', '1', '1516243571', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('23', 'admin', '285c37ed59286460ac1694aa5105497a.jpg', '\\uploads\\store\\drug_thumb\\20180118\\285c37ed59286460ac1694aa5105497a.jpg', '39098', 'jpg', '1', '127.0.0.1', '1', '1516244072', '1', '1516244072', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('24', 'store', 'a3e511e2ea3b12fdc26a03041bedd606.jpg', '\\uploads\\store\\drug_thumb\\20180118\\a3e511e2ea3b12fdc26a03041bedd606.jpg', '39098', 'jpg', '0', '127.0.0.1', '1', '1516244349', '0', '1516244349', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('25', 'store', 'bf6279617c9943d0514ffddb016a7c56.jpg', '\\uploads\\store\\drug_thumb\\20180118\\bf6279617c9943d0514ffddb016a7c56.jpg', '39098', 'jpg', '0', '127.0.0.1', '1', '1516244497', '0', '1516244497', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('26', 'store', 'abf7795d659ce43714239bb9b5927f09.jpg', '\\uploads\\store\\drug_thumb\\20180118\\abf7795d659ce43714239bb9b5927f09.jpg', '39098', 'jpg', '0', '127.0.0.1', '1', '1516244644', '0', '1516244644', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('27', 'store', 'b4b178a29f74f6372bf25faaa7b2aacb.jpg', '\\uploads\\store\\drug_thumb\\20180118\\b4b178a29f74f6372bf25faaa7b2aacb.jpg', '39098', 'jpg', '0', '127.0.0.1', '1', '1516244736', '0', '1516244736', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('28', 'store', 'cd9db86b5f77f4e074234be2b6164d2c.jpg', '\\uploads\\store\\drug_thumb\\20180118\\cd9db86b5f77f4e074234be2b6164d2c.jpg', '39098', 'jpg', '0', '127.0.0.1', '1', '1516244836', '0', '1516244836', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('29', 'store', '34e9ebc593b9330130ff6350fabb1ec0.jpg', '\\uploads\\store\\drug_thumb\\20180118\\34e9ebc593b9330130ff6350fabb1ec0.jpg', '39098', 'jpg', '0', '127.0.0.1', '1', '1516244850', '0', '1516244850', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('30', 'store', '3b9769a5c9b62fd24bf778876fa69ba3.jpg', '\\uploads\\store\\drug_thumb\\20180118\\3b9769a5c9b62fd24bf778876fa69ba3.jpg', '39098', 'jpg', '0', '127.0.0.1', '1', '1516244868', '0', '1516244868', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('31', 'store', '175e1e5b730fb8e5f09449addc236c7b.jpg', '\\uploads\\store\\drug_thumb\\20180118\\175e1e5b730fb8e5f09449addc236c7b.jpg', '39098', 'jpg', '0', '127.0.0.1', '1', '1516244924', '0', '1516244924', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('32', 'store', '025bde3a3ebadeaeafa2f72535598098.jpg', '\\uploads\\store\\drug_thumb\\20180118\\025bde3a3ebadeaeafa2f72535598098.jpg', '39098', 'jpg', '0', '127.0.0.1', '1', '1516245105', '0', '1516245105', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('33', 'admin', '6ede2fafe51263c716e5449c05d83cb4.jpg', '\\uploads\\admin\\drug_thumb\\20180118\\6ede2fafe51263c716e5449c05d83cb4.jpg', '39098', 'jpg', '1', '127.0.0.1', '1', '1516245136', '1', '1516245136', 'drug_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('34', 'admin', '5fba97cad4192725b003a0e2b90f3668.jpg', '\\uploads\\admin\\drug_thumb\\20180118\\5fba97cad4192725b003a0e2b90f3668.jpg', '39098', 'jpg', '2', '127.0.0.1', '1', '1516268457', '2', '1516268457', 'drug_thumb', '0');

-- ----------------------------
-- Table structure for `tplay_drug`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_drug`;
CREATE TABLE `tplay_drug` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `drug_store_id` int(11) DEFAULT NULL COMMENT '所属药企id',
  `drug_store_house_id` int(11) DEFAULT '0' COMMENT '所属药企仓库id',
  `drug_store_house_name` varchar(255) DEFAULT NULL COMMENT '所属药企仓库名称',
  `drug_name` varchar(255) DEFAULT NULL COMMENT '药品名称',
  `drug_alias` varchar(255) DEFAULT NULL COMMENT '别名',
  `drug_pinyin_code` varchar(255) DEFAULT NULL COMMENT '拼音码',
  `drug_is_insurance` int(1) DEFAULT '1' COMMENT '是否纳入医保[1:未纳入 2:已纳入]',
  `drug_thumb` varchar(255) DEFAULT NULL COMMENT '药品图片',
  `drug_price` decimal(11,2) DEFAULT '0.00' COMMENT '药品价格',
  `drug_factory` varchar(255) DEFAULT NULL COMMENT '药品厂家',
  `drug_ingredients` text COMMENT '药品成分',
  `drug_functions` text COMMENT '功能主治',
  `drug_dosage` varchar(500) DEFAULT NULL COMMENT '用法用量',
  `drug_contraindication` varchar(500) DEFAULT NULL COMMENT '禁忌症',
  `drug_insurance_type` varchar(255) DEFAULT NULL COMMENT '医保类型[1:社会医疗保险 2:商业医疗保险]',
  `drug_attribute` varchar(255) DEFAULT NULL COMMENT '药品属性',
  `drug_considerations` varchar(500) DEFAULT NULL COMMENT '注意事项',
  `drug_indication` varchar(500) DEFAULT NULL COMMENT '适应症',
  `drug_pharmacological` varchar(500) DEFAULT NULL COMMENT '药理作用',
  `drug_unreaction` varchar(500) DEFAULT NULL COMMENT '不良反应',
  `drug_interaction` varchar(500) DEFAULT NULL COMMENT '药物相互作用',
  `drug_storage` varchar(500) DEFAULT NULL COMMENT '贮藏',
  `drug_expiry_date` varchar(255) DEFAULT NULL COMMENT '有效期',
  `drug_license_number` varchar(500) DEFAULT NULL COMMENT '批准文号',
  `drug_manufacturing_enterprise` varchar(500) DEFAULT NULL COMMENT '生产企业',
  `drug_agent_cate_id` int(11) DEFAULT NULL COMMENT '药品剂型ID',
  `drug_agent_cate_name` varchar(255) DEFAULT NULL COMMENT '药品剂型名称',
  `drug_nature_cate_id` int(11) DEFAULT NULL COMMENT '药品性质ID',
  `drug_nature_cate_name` varchar(255) DEFAULT NULL COMMENT '药品性质名称',
  `drug_market_cate_id` int(11) DEFAULT NULL COMMENT '药品目标市场ID',
  `drug_market_cate_name` varchar(255) DEFAULT NULL COMMENT '药品目标市场名称',
  `drug_use_cate_id` int(11) DEFAULT NULL COMMENT '药品用途ID',
  `drug_use_cate_name` varchar(255) DEFAULT NULL COMMENT '药品用途名称',
  `is_otc` int(1) DEFAULT '1' COMMENT '是否非处方药[1:非处方药 2:处方药]',
  `status` int(1) DEFAULT '0' COMMENT '上架状态[0:仓库中 1:已上架 2:已下架]',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='药品数据表';

-- ----------------------------
-- Records of tplay_drug
-- ----------------------------
INSERT INTO `tplay_drug` VALUES ('1', '0', '1', null, '999感冒灵颗粒', '感冒灵颗粒', 'SJGML999', '2', '3', '21.00', '华润三九医药股份有限公司', '三叉苦、岗梅、金盏银盘、薄荷油、野菊花、马来酸氯苯那敏、咖啡因、对乙酰氨基酚。辅料为蔗糖粉。', '本品解热镇痛。用于感冒引起的头痛，发热等', '开水冲服，一次1袋，一日3次', '严重肝肾功能不全者禁用', '1', '浅棕色至深棕色的颗粒，味甜、微苦。', '1.·忌烟，酒及辛辣，生冷，油腻食物。', '本品解热镇痛。用于感冒引起的头痛，发热，鼻塞，流涕，咽痛等[2]  。', '', '偶见皮疹、荨麻疹、药热及粒细胞减少等', '', '', '2020-02-29', '国药准字Z44021940', '华润三九医药股份有限公司', '1', '', '1', null, '1', null, '1', null, '2', '1');
INSERT INTO `tplay_drug` VALUES ('2', '0', '0', null, '999感冒灵颗粒', '感冒灵颗粒', 'SJGML999', '2', '3', '21.00', '华润三九医药股份有限公司', '三叉苦、岗梅、金盏银盘、薄荷油、野菊花、马来酸氯苯那敏、咖啡因、对乙酰氨基酚。辅料为蔗糖粉。', '本品解热镇痛。用于感冒引起的头痛，发热等', '开水冲服，一次1袋，一日3次', '严重肝肾功能不全者禁用', '1', '浅棕色至深棕色的颗粒，味甜、微苦。', '1.·忌烟，酒及辛辣，生冷，油腻食物。', '本品解热镇痛。用于感冒引起的头痛，发热，鼻塞，流涕，咽痛等[2]  。', '', '偶见皮疹、荨麻疹、药热及粒细胞减少等', '', '', '2020-02-29', '国药准字Z44021940', '华润三九医药股份有限公司', null, null, null, null, null, null, null, null, '2', '1');

-- ----------------------------
-- Table structure for `tplay_drug_agent_cate`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_drug_agent_cate`;
CREATE TABLE `tplay_drug_agent_cate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '药品剂型名称',
  `create_time` int(11) DEFAULT NULL COMMENT '发布时间',
  `publisher` varchar(255) DEFAULT 'admin' COMMENT '发布人',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '使用状态 0:禁用 1:启用',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='药品剂型分类表';

-- ----------------------------
-- Records of tplay_drug_agent_cate
-- ----------------------------
INSERT INTO `tplay_drug_agent_cate` VALUES ('1', '颗粒剂', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_agent_cate` VALUES ('2', '丸剂', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_agent_cate` VALUES ('3', '口服散剂', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_agent_cate` VALUES ('4', '酊剂', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_agent_cate` VALUES ('5', '片剂', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_agent_cate` VALUES ('6', '胶囊剂', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_agent_cate` VALUES ('7', '口服液体剂', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_agent_cate` VALUES ('8', '外用散剂', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_agent_cate` VALUES ('9', '软膏剂', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_agent_cate` VALUES ('10', '贴剂', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_agent_cate` VALUES ('11', '外用液体剂', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_agent_cate` VALUES ('12', '硬膏剂', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_agent_cate` VALUES ('13', '凝胶剂', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_agent_cate` VALUES ('14', '涂剂', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_agent_cate` VALUES ('15', '栓剂', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_agent_cate` VALUES ('16', '滴眼剂', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_agent_cate` VALUES ('17', '滴耳剂', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_agent_cate` VALUES ('18', '滴鼻剂', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_agent_cate` VALUES ('19', '吸入剂', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_agent_cate` VALUES ('20', '注射剂', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_agent_cate` VALUES ('21', '其他', '1516176492', 'admin', '1');

-- ----------------------------
-- Table structure for `tplay_drug_nature_cate`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_drug_nature_cate`;
CREATE TABLE `tplay_drug_nature_cate` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '药品类型id',
  `name` varchar(100) DEFAULT NULL COMMENT '药品类型名称',
  `create_time` int(11) DEFAULT NULL COMMENT '发布时间',
  `publisher` varchar(255) DEFAULT 'admin' COMMENT '发布人',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '使用状态 0:禁用 1:启用',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='药品性质分类表';

-- ----------------------------
-- Records of tplay_drug_nature_cate
-- ----------------------------
INSERT INTO `tplay_drug_nature_cate` VALUES ('1', '中药材', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_nature_cate` VALUES ('2', '中药饮片', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_nature_cate` VALUES ('3', '中成药', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_nature_cate` VALUES ('4', '中西成药', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_nature_cate` VALUES ('5', '化学原料药及其制剂', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_nature_cate` VALUES ('6', '抗生素', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_nature_cate` VALUES ('7', '生化药品', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_nature_cate` VALUES ('8', '放射性药品', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_nature_cate` VALUES ('9', '血清', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_nature_cate` VALUES ('10', '疫苗', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_nature_cate` VALUES ('11', '血液制品', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_nature_cate` VALUES ('12', '诊断药品', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_nature_cate` VALUES ('13', '其他', '1516176492', 'admin', '1');

-- ----------------------------
-- Table structure for `tplay_drug_target_market_cate`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_drug_target_market_cate`;
CREATE TABLE `tplay_drug_target_market_cate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '药品市场类型名称',
  `create_time` int(11) DEFAULT NULL COMMENT '发布时间',
  `publisher` varchar(255) DEFAULT 'admin' COMMENT '发布人',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '使用状态 0:禁用 1:启用',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='药品市场类型分类表';

-- ----------------------------
-- Records of tplay_drug_target_market_cate
-- ----------------------------
INSERT INTO `tplay_drug_target_market_cate` VALUES ('1', '零售推广药品', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_target_market_cate` VALUES ('2', '临床推广药品', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_target_market_cate` VALUES ('3', '其他', '1516176492', 'admin', '1');

-- ----------------------------
-- Table structure for `tplay_drug_use_cate`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_drug_use_cate`;
CREATE TABLE `tplay_drug_use_cate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '药品用途名称',
  `create_time` int(11) DEFAULT NULL COMMENT '发布时间',
  `publisher` varchar(255) DEFAULT 'admin' COMMENT '发布人',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '使用状态 0:禁用 1:启用',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='药品用途分类表';

-- ----------------------------
-- Records of tplay_drug_use_cate
-- ----------------------------
INSERT INTO `tplay_drug_use_cate` VALUES ('1', '抗生素类药品', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_use_cate` VALUES ('2', '心脑血管用药', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_use_cate` VALUES ('3', '消化系统用药', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_use_cate` VALUES ('4', '呼吸系统用药', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_use_cate` VALUES ('5', '泌尿系统用药', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_use_cate` VALUES ('6', '血液系统用药', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_use_cate` VALUES ('7', '五官科用药', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_use_cate` VALUES ('8', '抗风湿类药品', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_use_cate` VALUES ('9', '注射剂类药品', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_use_cate` VALUES ('10', '糖尿病用药', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_use_cate` VALUES ('11', '激素类药品', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_use_cate` VALUES ('12', '皮肤科用药', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_use_cate` VALUES ('13', '妇科用药', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_use_cate` VALUES ('14', '抗肿瘤用药', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_use_cate` VALUES ('15', '抗精神病药品', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_use_cate` VALUES ('16', '清热解毒药品', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_use_cate` VALUES ('17', '受体激动\\阻断药和抗过敏药', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_use_cate` VALUES ('18', '滋补类药品', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_use_cate` VALUES ('19', '维生素、矿物质药品', '1516176492', 'admin', '1');
INSERT INTO `tplay_drug_use_cate` VALUES ('20', '其他', '1516176492', 'admin', '1');

-- ----------------------------
-- Table structure for `tplay_emailconfig`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_emailconfig`;
CREATE TABLE `tplay_emailconfig` (
  `email` varchar(5) NOT NULL COMMENT '邮箱配置标识',
  `from_email` varchar(50) NOT NULL COMMENT '邮件来源也就是邮件地址',
  `from_name` varchar(50) NOT NULL,
  `smtp` varchar(50) NOT NULL COMMENT '邮箱smtp服务器',
  `username` varchar(100) NOT NULL COMMENT '邮箱账号',
  `password` varchar(100) NOT NULL COMMENT '邮箱密码',
  `title` varchar(200) NOT NULL COMMENT '邮件标题',
  `content` text NOT NULL COMMENT '邮件模板',
  KEY `email` (`email`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tplay_emailconfig
-- ----------------------------
INSERT INTO `tplay_emailconfig` VALUES ('email', '', '', '', '', '', '', '');

-- ----------------------------
-- Table structure for `tplay_messages`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_messages`;
CREATE TABLE `tplay_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` int(11) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `is_look` int(1) NOT NULL DEFAULT '0' COMMENT '0未读1已读',
  `message` text NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tplay_messages
-- ----------------------------

-- ----------------------------
-- Table structure for `tplay_pharmacy`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_pharmacy`;
CREATE TABLE `tplay_pharmacy` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `pharmacy_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '药店名称',
  `pharmacy_CUFCC` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '统一社会信用代码',
  `pharmacy_organization_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '组织机构代码',
  `pharmacy_registration_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '注册号',
  `pharmacy_operating_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '经营状态',
  `pharmacy_industry` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '所属行业',
  `pharmacy_establishment_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '成立日期',
  `pharmacy_business_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '公司类型',
  `pharmacy_operation_period` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '营业期限',
  `pharmacy_legal_representative` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '法定代表人',
  `pharmacy_isuse_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '发照日期',
  `pharmacy_registered_capital` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '注册资本',
  `pharmacy_registration_authority` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '登记机关',
  `pharmacy_business_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '药店地址',
  `pharmacy_scope_business` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '经营范围',
  `pharmacy_contact_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '药店联系电话',
  `pharmacy_contact_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '药店邮箱',
  `pharmacy_licence_picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '营业执照图片',
  `pharmacy_account_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款人(银行开户名称)',
  `pharmacy_account_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款账号',
  `pharmacy_account_cash_bank` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款银行',
  `pharmacy_account_cash_bank_branch` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款支行名称',
  `pharmacy_create_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '申请时间',
  `pharmacy_auth_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '认证时间',
  `pharmacy_auth_status` int(11) DEFAULT '1' COMMENT '认证状态[1:认证中 2:认证通过 3:认证失败]',
  `pharmacy_siteurl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '药店官网',
  `pharmacy_intro` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '药店简介',
  `pharmacy_longitude` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '经度',
  `pharmacy_latitude` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '纬度',
  `pharmacy_province_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '省名称',
  `pharmacy_province_id` int(11) DEFAULT '0' COMMENT '省id',
  `pharmacy_city_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '市名称',
  `pharmacy_city_id` int(11) DEFAULT '0' COMMENT '市id',
  `pharmacy_district_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '区名称',
  `pharmacy_district_id` int(11) DEFAULT '0' COMMENT '区id',
  `pharmacy_road_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '街道名称',
  `pharmacy_road_id` int(11) DEFAULT '0' COMMENT '街道id',
  `remark` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  `status` int(11) DEFAULT '1' COMMENT '启用状态[1:启用 2:禁用]',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='药店信息';

-- ----------------------------
-- Records of tplay_pharmacy
-- ----------------------------
INSERT INTO `tplay_pharmacy` VALUES ('1', '西安怡康医药连锁有限责任公司', '9161010473507449XG', '73507449X', '610100100044932', '存续', '零售业', '2002-07-22', '有限责任公司', '2002年7月22日 - 2032年6月2日', '何煜', '2017-10-19', '7000万', '西安市工商行政管理局', '西安市大庆路副24号', '化学药制剂、中成药、生化药品、抗生素、中药材、中药饮片、生物制品（除疫苗）的零售；第一类、二类、三类医疗器械的零售；预包装食品、散装食品、婴幼儿配方乳粉、其他婴幼儿配方食品、保健食品、特殊医学用途配方食品的销售；中医科；推拿按摩；水电费、宽带费、电话费代缴；城市一卡通IC卡的发售、充值代缴；商业预付卡的代理及发售；计算机软硬件、照相器材、电讯器材、照明器材、孕妇婴幼儿用品、儿童玩具、服装鞋帽、消毒用品、计生用品、针纺织品、化妆品、日用百货、洗涤用品、健身器械、电子产品、家用电器、五金交电、皮革、塑料制品、办公文化用品、保健用品、农副产品、眼镜的销售；场地租赁；医药信息咨询服务、健康管理咨询服务。（依法须经批准的项目，经相关部门批准后方可开展经营活动）', '029-87513026', '', '5', '', '', '', '', '2018-01-04', null, '1', 'http://www.ykyyls.com/', '怡康医药集团成立于2001年，是以医药零售为核心的多元化集团企业，旗下专业子公司15家（医药批发、怡康璞太和中医馆、中药饮片厂、玖和便利连锁超市、怡康婴乐宝母婴用品连锁店等）；2004—2010年连续七年，西安怡康连锁位列“中国零售连锁药店三十强” 2005年被定为陕西省医药行业政府重点扶持企业，2006年获得“十一五”陕西最具活力民营企业奖。2009年1月在“2008中国西安一经济影响力”评选中，怡康医药荣获\"西安十大最具经济影响力品牌”，2009年1 2月，荣获“2009年度西安经济最具成长性企业”， 2010年11月，“怡康”被评为陕西省著名商标，2012年4月，“怡康”被评为中国连锁百强综合实力排名16名，西北区域第1名，2012年12月，被评为陕西区域最具影响力企业奖，2012年12月，被评为陕西最具推动力公益企业。目前零售门店500多家，员工超过5000人。因公司发展需求，面向社会诚聘有理想、有抱负，与企业共创辉煌的精英人才。应聘地址：西安市大庆路付24号（乘坐公交车611路、103路到终点站汉城路下车，十字路口往东走100米路北即到）联系电话：84618261 84622', null, null, null, '0', null, '0', null, '0', null, '0', '', '1');

-- ----------------------------
-- Table structure for `tplay_smsconfig`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_smsconfig`;
CREATE TABLE `tplay_smsconfig` (
  `sms` varchar(10) NOT NULL DEFAULT 'sms' COMMENT '标识',
  `appkey` varchar(200) NOT NULL,
  `secretkey` varchar(200) NOT NULL,
  `type` varchar(100) DEFAULT 'normal' COMMENT '短信类型',
  `name` varchar(100) NOT NULL COMMENT '短信签名',
  `code` varchar(100) NOT NULL COMMENT '短信模板ID',
  `content` text NOT NULL COMMENT '短信默认模板',
  KEY `sms` (`sms`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tplay_smsconfig
-- ----------------------------
INSERT INTO `tplay_smsconfig` VALUES ('sms', '', '', '', '', '', '');

-- ----------------------------
-- Table structure for `tplay_store`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_store`;
CREATE TABLE `tplay_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `store_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '供应商名称',
  `store_CUFCC` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '统一社会信用代码',
  `store_organization_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '组织机构代码',
  `store_registration_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '注册号',
  `store_operating_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '经营状态',
  `store_industry` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '所属行业',
  `store_establishment_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '成立日期',
  `store_business_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '公司类型',
  `store_operation_period` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '营业期限',
  `store_legal_representative` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '法定代表人',
  `store_isuse_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '发照日期',
  `store_registered_capital` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '注册资本',
  `store_registration_authority` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '登记机关',
  `store_business_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '企业地址',
  `store_scope_business` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '经营范围',
  `store_contact_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '企业联系电话',
  `store_contact_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '企业邮箱',
  `store_licence_picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '营业执照图片',
  `store_account_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款人(银行开户名称)',
  `store_account_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款账号',
  `store_account_cash_bank` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款银行',
  `store_account_cash_bank_branch` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款支行名称',
  `store_create_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '申请时间',
  `store_auth_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '认证时间',
  `store_auth_status` int(11) DEFAULT '1' COMMENT '认证状态[1:认证中 2:认证通过 3:认证失败]',
  `store_siteurl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '企业官网',
  `store_intro` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '公司简介',
  `store_longitude` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '经度',
  `store_latitude` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '纬度',
  `store_province_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '省名称',
  `store_province_id` int(11) DEFAULT '0' COMMENT '省id',
  `store_city_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '市名称',
  `store_city_id` int(11) DEFAULT '0' COMMENT '市id',
  `store_district_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '区名称',
  `store_district_id` int(11) DEFAULT '0' COMMENT '区id',
  `store_road_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '街道名称',
  `store_road_id` int(11) DEFAULT '0' COMMENT '街道id',
  `remark` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  `status` int(11) DEFAULT '1' COMMENT '启用状态[1:启用 2:禁用]',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='医药供货企业信息';

-- ----------------------------
-- Records of tplay_store
-- ----------------------------
INSERT INTO `tplay_store` VALUES ('1', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '5', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('2', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '5', '', '', '', '', '2018-01-04', null, '2', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '2');
INSERT INTO `tplay_store` VALUES ('3', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '5', '', '', '', '', '2018-01-04', null, '3', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('4', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '5', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('5', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '5', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('6', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '5', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('7', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '5', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('8', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '5', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('9', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '5', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('10', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '5', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('11', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '5', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('12', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '5', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('13', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '5', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('14', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '5', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('15', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '5', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('16', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '5', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('17', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '5', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('18', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '5', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('19', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '5', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('20', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '5', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');

-- ----------------------------
-- Table structure for `tplay_store_warehouse`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_store_warehouse`;
CREATE TABLE `tplay_store_warehouse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL COMMENT '药企id',
  `warehouse_name` varchar(100) DEFAULT NULL COMMENT '仓库名称',
  `warehouse_contacts` varchar(100) DEFAULT NULL COMMENT '仓库负责人',
  `warehouse_contacts_tel` varchar(100) DEFAULT NULL COMMENT '仓库负责人联系方式',
  `warehouse_longitude` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '经度',
  `warehouse_latitude` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '纬度',
  `warehouse_province_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '省名称',
  `warehouse_province_id` int(11) DEFAULT '0' COMMENT '省id',
  `warehouse_city_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '市名称',
  `warehouse_city_id` int(11) DEFAULT '0' COMMENT '市id',
  `warehouse_district_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '区名称',
  `warehouse_district_id` int(11) DEFAULT '0' COMMENT '区id',
  `warehouse_road_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '街道名称',
  `warehouse_road_id` int(11) DEFAULT '0' COMMENT '街道id',
  `warehouse_address` varchar(100) DEFAULT NULL COMMENT '仓库具体地址',
  `warehouse_remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '使用状态 0:禁用 1:启用',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='药企仓库表';

-- ----------------------------
-- Records of tplay_store_warehouse
-- ----------------------------
INSERT INTO `tplay_store_warehouse` VALUES ('1', '1', '西安城东仓库', null, null, null, null, null, '0', null, '0', null, '0', null, '0', null, null, '1');
INSERT INTO `tplay_store_warehouse` VALUES ('2', '1', '西安城西仓库', null, null, null, null, null, '0', null, '0', null, '0', null, '0', null, null, '1');
INSERT INTO `tplay_store_warehouse` VALUES ('3', '1', '西安城南仓库', null, null, null, null, null, '0', null, '0', null, '0', null, '0', null, null, '1');
INSERT INTO `tplay_store_warehouse` VALUES ('4', '1', '西安城北仓库', null, null, null, null, null, '0', null, '0', null, '0', null, '0', null, null, '1');

-- ----------------------------
-- Table structure for `tplay_urlconfig`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_urlconfig`;
CREATE TABLE `tplay_urlconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aliases` varchar(200) NOT NULL COMMENT '想要设置的别名',
  `url` varchar(200) NOT NULL COMMENT '原url结构',
  `desc` text COMMENT '备注',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '0禁用1使用',
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE,
  KEY `status` (`status`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tplay_urlconfig
-- ----------------------------

-- ----------------------------
-- Table structure for `tplay_webconfig`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_webconfig`;
CREATE TABLE `tplay_webconfig` (
  `web` varchar(20) NOT NULL COMMENT '网站配置标识',
  `name` varchar(200) NOT NULL COMMENT '网站名称',
  `keywords` text COMMENT '关键词',
  `desc` text COMMENT '描述',
  `is_log` int(1) NOT NULL DEFAULT '1' COMMENT '1开启日志0关闭',
  `file_type` varchar(200) DEFAULT NULL COMMENT '允许上传的类型',
  `file_size` bigint(20) DEFAULT NULL COMMENT '允许上传的最大值',
  `statistics` text COMMENT '统计代码',
  `black_ip` text COMMENT 'ip黑名单',
  `url_suffix` varchar(20) DEFAULT NULL COMMENT 'url伪静态后缀',
  KEY `web` (`web`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tplay_webconfig
-- ----------------------------
INSERT INTO `tplay_webconfig` VALUES ('web', 'Eyao 订货系统管理后台', 'Tplay,后台管理,thinkphp5,layui', 'Tplay是一款基于ThinkPHP5.0.12 + layui2.2.45 + ECharts + Mysql开发的后台管理框架，集成了一般应用所必须的基础性功能，为开发者节省大量的时间。', '1', 'jpg,png,gif,mp4,zip,jpeg', '500', '', '', null);
