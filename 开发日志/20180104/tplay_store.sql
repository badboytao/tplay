/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50553
Source Host           : 127.0.0.1:3306
Source Database       : 94park

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-01-04 15:51:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tplay_store`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_store`;
CREATE TABLE `tplay_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `store_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '供应商名称',
  `store_CUFCC` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '统一社会信用代码',
  `store_organization_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '组织机构代码',
  `store_registration_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '注册号',
  `store_operating_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '经营状态',
  `store_industry` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '所属行业',
  `store_establishment_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '成立日期',
  `store_business_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '公司类型',
  `store_operation_period` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '营业期限',
  `store_legal_representative` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '法定代表人',
  `store_isuse_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '发照日期',
  `store_registered_capital` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '注册资本',
  `store_registration_authority` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '登记机关',
  `store_business_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '企业地址',
  `store_scope_business` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '经营范围',
  `store_contact_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '企业联系电话',
  `store_contact_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '企业邮箱',
  `store_licence_picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '营业执照图片',
  `store_account_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款人(银行开户名称)',
  `store_account_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款账号',
  `store_account_cash_bank` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款银行',
  `store_account_cash_bank_branch` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款支行名称',
  `store_create_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '申请时间',
  `store_auth_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '认证时间',
  `store_auth_status` int(11) DEFAULT '1' COMMENT '认证状态[1:认证中 2:认证通过 3:认证失败]',
  `store_siteurl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '企业官网',
  `store_intro` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '公司简介',
  `status` int(11) DEFAULT '1' COMMENT '启用状态[1:启用 2:禁用]',
  `remark` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='医药供货企业信息';

-- ----------------------------
-- Records of tplay_store
-- ----------------------------
INSERT INTO `tplay_store` VALUES ('1', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '9', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', '1', '');
INSERT INTO `tplay_store` VALUES ('2', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '9', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', '1', '');
