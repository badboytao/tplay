/* 建立药企数据表 */

CREATE TABLE `tplay_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `store_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '供应商名称',
  `store_CUFCC` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '统一社会信用代码',
  `store_organization_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '组织机构代码',
  `store_registration_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '注册号',
  `store_operating_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '经营状态',
  `store_industry` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '所属行业',
  `store_establishment_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `store_business_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '公司类型',
  `store_operation_period` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '营业期限',
  `store_legal_representative` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '法定代表人',
  `store_isuse_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '发照日期',
  `store_registered_capital` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '注册资本',
  `store_registration_authority` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '登记机关',
  `store_business_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '企业地址',
  `store_scope_business` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '经营范围',
  `store_contact_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '企业联系电话',
  `store_contact_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '企业邮箱',
  `store_licence_picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '营业执照图片',
  `store_account_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款人(银行开户名称)',
  `store_account_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款账号',
  `store_account_cash_bank` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款银行',
  `store_account_cash_bank_branch` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款支行名称',
  `store_create_time` int(11) DEFAULT NULL COMMENT '申请时间',
  `store_auth_time` int(11) DEFAULT NULL COMMENT '认证时间',
  `store_auth_status` int(11) DEFAULT '1' COMMENT '状态[1:认证中 2:认证通过 3:认证失败]',
  `status` int(11) DEFAULT '1' COMMENT '状态[1:启用 2:禁用]',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='医药供货企业信息';




/* 建立药店数据表 */

