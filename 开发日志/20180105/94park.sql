/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50553
Source Host           : 127.0.0.1:3306
Source Database       : 94park

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-01-05 17:33:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tplay_admin`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_admin`;
CREATE TABLE `tplay_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(20) DEFAULT NULL COMMENT '昵称',
  `name` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `thumb` int(11) NOT NULL DEFAULT '1' COMMENT '管理员头像',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '修改时间',
  `login_time` int(11) DEFAULT NULL COMMENT '最后登录时间',
  `login_ip` varchar(100) DEFAULT NULL COMMENT '最后登录ip',
  `admin_cate_id` int(2) NOT NULL DEFAULT '1' COMMENT '管理员分组',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tplay_admin
-- ----------------------------
INSERT INTO `tplay_admin` VALUES ('1', '晓月老板', 'admin', '31c64b511d1e90fcda8519941c1bd660', '2', '1510885948', '1514964314', '1515135104', '127.0.0.1', '1');

-- ----------------------------
-- Table structure for `tplay_admin_cate`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_admin_cate`;
CREATE TABLE `tplay_admin_cate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `permissions` text COMMENT '权限菜单',
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tplay_admin_cate
-- ----------------------------
INSERT INTO `tplay_admin_cate` VALUES ('1', '超级管理员', '1,32,33,34,4,5,7,8,6,30,29,9,10,23,31', '0', '1511233197');

-- ----------------------------
-- Table structure for `tplay_admin_log`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_admin_log`;
CREATE TABLE `tplay_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_menu_id` int(11) NOT NULL COMMENT '操作菜单id',
  `admin_id` int(11) NOT NULL COMMENT '操作者id',
  `ip` varchar(100) DEFAULT NULL COMMENT '操作ip',
  `operation_id` varchar(200) DEFAULT NULL COMMENT '操作关联id',
  `create_time` int(11) NOT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tplay_admin_log
-- ----------------------------
INSERT INTO `tplay_admin_log` VALUES ('1', '66', '1', '127.0.0.1', '', '1514958668');
INSERT INTO `tplay_admin_log` VALUES ('2', '73', '1', '127.0.0.1', '1', '1514961796');
INSERT INTO `tplay_admin_log` VALUES ('3', '77', '1', '127.0.0.1', '85', '1514963476');
INSERT INTO `tplay_admin_log` VALUES ('4', '77', '1', '127.0.0.1', '85', '1514963617');
INSERT INTO `tplay_admin_log` VALUES ('5', '77', '1', '127.0.0.1', '86', '1514963808');
INSERT INTO `tplay_admin_log` VALUES ('6', '77', '1', '127.0.0.1', '86', '1514963833');
INSERT INTO `tplay_admin_log` VALUES ('7', '77', '1', '127.0.0.1', '1', '1514963853');
INSERT INTO `tplay_admin_log` VALUES ('8', '77', '1', '127.0.0.1', '86', '1514963925');
INSERT INTO `tplay_admin_log` VALUES ('9', '77', '1', '127.0.0.1', '87', '1514964135');
INSERT INTO `tplay_admin_log` VALUES ('10', '77', '1', '127.0.0.1', '88', '1514964215');
INSERT INTO `tplay_admin_log` VALUES ('11', '77', '1', '127.0.0.1', '88', '1514964264');
INSERT INTO `tplay_admin_log` VALUES ('12', '69', '1', '127.0.0.1', '2', '1514964313');
INSERT INTO `tplay_admin_log` VALUES ('13', '2', '1', '127.0.0.1', '1', '1514964314');
INSERT INTO `tplay_admin_log` VALUES ('14', '77', '1', '127.0.0.1', '88', '1514964628');
INSERT INTO `tplay_admin_log` VALUES ('15', '66', '1', '127.0.0.1', '', '1514966852');
INSERT INTO `tplay_admin_log` VALUES ('16', '77', '1', '127.0.0.1', '86', '1514967752');
INSERT INTO `tplay_admin_log` VALUES ('17', '77', '1', '127.0.0.1', '88', '1514967765');
INSERT INTO `tplay_admin_log` VALUES ('18', '77', '1', '127.0.0.1', '89', '1514968013');
INSERT INTO `tplay_admin_log` VALUES ('19', '77', '1', '127.0.0.1', '90', '1514968094');
INSERT INTO `tplay_admin_log` VALUES ('20', '77', '1', '127.0.0.1', '91', '1514968243');
INSERT INTO `tplay_admin_log` VALUES ('21', '77', '1', '127.0.0.1', '89', '1514968424');
INSERT INTO `tplay_admin_log` VALUES ('22', '66', '1', '127.0.0.1', '', '1515042806');
INSERT INTO `tplay_admin_log` VALUES ('23', '69', '1', '127.0.0.1', '3', '1515047495');
INSERT INTO `tplay_admin_log` VALUES ('24', '69', '1', '127.0.0.1', '4', '1515047640');
INSERT INTO `tplay_admin_log` VALUES ('25', '69', '1', '127.0.0.1', '5', '1515048053');
INSERT INTO `tplay_admin_log` VALUES ('26', '69', '1', '127.0.0.1', '6', '1515048670');
INSERT INTO `tplay_admin_log` VALUES ('27', '69', '1', '127.0.0.1', '7', '1515049155');
INSERT INTO `tplay_admin_log` VALUES ('28', '69', '1', '127.0.0.1', '8', '1515049689');
INSERT INTO `tplay_admin_log` VALUES ('29', '66', '1', '127.0.0.1', '', '1515050188');
INSERT INTO `tplay_admin_log` VALUES ('30', '69', '1', '127.0.0.1', '9', '1515051722');
INSERT INTO `tplay_admin_log` VALUES ('31', '91', '1', '127.0.0.1', '2', '1515051942');
INSERT INTO `tplay_admin_log` VALUES ('32', '66', '1', '127.0.0.1', '', '1515057534');
INSERT INTO `tplay_admin_log` VALUES ('33', '77', '1', '127.0.0.1', '92', '1515059118');
INSERT INTO `tplay_admin_log` VALUES ('34', '77', '1', '127.0.0.1', '93', '1515059175');
INSERT INTO `tplay_admin_log` VALUES ('35', '77', '1', '127.0.0.1', '94', '1515059237');
INSERT INTO `tplay_admin_log` VALUES ('36', '77', '1', '127.0.0.1', '92', '1515059266');
INSERT INTO `tplay_admin_log` VALUES ('37', '77', '1', '127.0.0.1', '88', '1515059290');
INSERT INTO `tplay_admin_log` VALUES ('38', '77', '1', '127.0.0.1', '93', '1515059300');
INSERT INTO `tplay_admin_log` VALUES ('39', '77', '1', '127.0.0.1', '94', '1515059310');
INSERT INTO `tplay_admin_log` VALUES ('40', '77', '1', '127.0.0.1', '88', '1515059469');
INSERT INTO `tplay_admin_log` VALUES ('41', '69', '1', '127.0.0.1', '10', '1515060246');
INSERT INTO `tplay_admin_log` VALUES ('42', '66', '1', '127.0.0.1', '', '1515082943');
INSERT INTO `tplay_admin_log` VALUES ('43', '66', '1', '127.0.0.1', '', '1515115149');
INSERT INTO `tplay_admin_log` VALUES ('44', '66', '1', '127.0.0.1', '', '1515135104');

-- ----------------------------
-- Table structure for `tplay_admin_menu`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_admin_menu`;
CREATE TABLE `tplay_admin_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `module` varchar(50) NOT NULL COMMENT '模块',
  `controller` varchar(100) NOT NULL COMMENT '控制器',
  `function` varchar(100) NOT NULL COMMENT '方法',
  `parameter` varchar(50) DEFAULT NULL COMMENT '参数',
  `description` varchar(250) DEFAULT NULL COMMENT '描述',
  `is_display` int(1) NOT NULL DEFAULT '1' COMMENT '1显示2隐藏',
  `type` int(1) NOT NULL DEFAULT '1' COMMENT '1权限+菜单2只作为菜单',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '上级菜单0为顶级菜单',
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `icon` varchar(100) DEFAULT NULL COMMENT '图标',
  `is_open` int(1) NOT NULL DEFAULT '0' COMMENT '0默认闭合1默认展开',
  `orders` int(11) NOT NULL DEFAULT '0' COMMENT '排序值，越小越靠前',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tplay_admin_menu
-- ----------------------------
INSERT INTO `tplay_admin_menu` VALUES ('1', '设置管理', 'admin', 'index', 'index', '', '管理软件的基础信息，包括个人的基本信息管理。', '1', '2', '0', '0', '1514963853', 'fa-cogs', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('2', '个人信息', 'admin', 'admin', 'personal', '', '对个人的一些信息进行管理。', '1', '2', '1', '0', '1513402673', 'fa-cog', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('4', '会员管理', 'admin', 'index', 'index', '', '后台管理员管理，包括后台权限组的管理。', '1', '2', '0', '1511015413', '1513558364', 'fa-user', '0', '2');
INSERT INTO `tplay_admin_menu` VALUES ('6', '角色分组', 'admin', 'admin', 'adminCate', '', '管理员角色分组管理。', '1', '2', '4', '1511083098', '1513412856', 'fa-group', '0', '2');
INSERT INTO `tplay_admin_menu` VALUES ('73', '添加/修改管理员', 'admin', 'admin', 'publish', '', '添加/修改管理员。', '2', '1', '72', '1513403009', '1513403009', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('74', '删除管理员', 'admin', 'admin', 'delete', '', '删除管理员。', '2', '1', '72', '1513403036', '1513403036', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('45', '日志管理', 'admin', 'index', 'index', '', '日志管理。', '1', '2', '0', '1511940197', '1513396527', 'fa-book', '0', '4');
INSERT INTO `tplay_admin_menu` VALUES ('77', '添加/修改菜单', 'admin', 'menu', 'publish', '', '添加/修改菜单。', '2', '1', '76', '1513403367', '1513403367', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('78', '删除菜单', 'admin', 'menu', 'delete', '', '删除菜单。', '2', '1', '76', '1513403393', '1513403393', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('30', '删除权限分组', 'admin', 'admin', 'adminCateDelete', '', '删除后台管理员权限分组。', '2', '1', '6', '1511227568', '1513396473', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('13', '修改密码', 'admin', 'admin', 'editPassword', '', '修改个人登录密码。', '1', '2', '1', '1511083565', '1513395989', 'fa-edit', '0', '2');
INSERT INTO `tplay_admin_menu` VALUES ('29', '添加/修改权限分组', 'admin', 'admin', 'adminCatePublish', '', '添加/修改管理员权限分组。', '2', '1', '6', '1511227503', '1513396481', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('79', '文件管理', 'admin', 'attachment', 'index', '', '文件管理。', '1', '2', '47', '1513403488', '1513404676', 'fa-file', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('33', '文件审核', 'admin', 'attachment', 'audit', '', '对文件进行审核。', '2', '1', '79', '1511227899', '1513403526', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('34', '文件删除', 'admin', 'attachment', 'delete', '', '对文件进行删除操作。', '2', '1', '79', '1511227936', '1513403541', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('35', '门户管理', 'admin', 'index', 'index', '', '门户内容管理', '1', '2', '0', '1511320705', '1513408714', 'fa-th', '0', '6');
INSERT INTO `tplay_admin_menu` VALUES ('36', '分类管理', 'admin', 'articlecate', 'index', '', '分类列表管理。', '1', '2', '35', '1511320748', '1513402018', 'fa-tags', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('37', '添加/修改分类', 'admin', 'articlecate', 'publish', '', '添加/修改分类操作。', '2', '1', '36', '1511320794', '1513402031', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('38', '删除分类', 'admin', 'articlecate', 'delete', '', '删除分类操作。', '2', '1', '36', '1511320824', '1513402041', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('39', '文章管理', 'admin', 'article', 'index', '', '文章列表管理', '1', '2', '35', '1511320850', '1513402055', 'fa-file-text', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('40', '添加/修改文章', 'admin', 'article', 'publish', '', '添加/修改文章操作。', '2', '1', '39', '1511320883', '1513402066', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('41', '删除文章', 'admin', 'article', 'delete', '', '删除文章操作。', '2', '1', '39', '1511320907', '1513402079', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('46', '操作日志', 'admin', 'admin', 'log', '', '管理员操作日志。', '1', '2', '45', '1511940227', '1513396537', 'fa-book', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('47', '数据管理', 'admin', 'index', 'index', '', '数据相关的管理。', '1', '2', '0', '1511940263', '1513402145', 'fa-cubes', '0', '5');
INSERT INTO `tplay_admin_menu` VALUES ('48', '数据库', 'admin', 'databackup', 'index', '', '数据库管理', '1', '2', '47', '1511940334', '1513402218', 'fa-database', '0', '2');
INSERT INTO `tplay_admin_menu` VALUES ('49', '数据库备份', 'admin', 'databackup', 'export', '', '数据库备份。', '2', '1', '48', '1511940383', '1513402229', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('50', '数据库优化', 'admin', 'databackup', 'optimize', '', '数据库优化。', '2', '1', '48', '1511940422', '1513402239', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('51', '数据库修复', 'admin', 'databackup', 'repair', '', '数据库修复', '2', '1', '48', '1511940450', '1513402248', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('52', '备份管理', 'admin', 'databackup', 'importlist', '', '数据库备份文件管理。', '1', '2', '47', '1511940505', '1513402265', 'fa-bookmark', '0', '3');
INSERT INTO `tplay_admin_menu` VALUES ('53', '数据库备份还原', 'admin', 'databackup', 'import', '', '数据库还原。', '2', '1', '52', '1511940554', '1513402275', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('54', '数据库备份删除', 'admin', 'databackup', 'del', '', '数据库备份删除。', '2', '1', '52', '1511940587', '1513402284', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('75', '菜单管理', 'admin', 'index', 'index', '', '菜单管理。', '1', '2', '0', '1513403151', '1513403151', 'fa-sitemap', '0', '3');
INSERT INTO `tplay_admin_menu` VALUES ('56', '邮件配置', 'admin', 'emailconfig', 'index', '', '邮件配置。', '1', '2', '1', '1512811551', '1513402539', 'fa-envelope', '0', '4');
INSERT INTO `tplay_admin_menu` VALUES ('57', '修改邮件配置', 'admin', 'emailconfig', 'publish', '', '修改邮件配置。', '2', '1', '56', '1512811595', '1513402369', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('58', '发送测试邮件', 'admin', 'emailconfig', 'mailto', '', '发送测试邮件。', '2', '1', '56', '1512811635', '1513402381', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('59', '短信配置', 'admin', 'smsconfig', 'index', '', '短信配置。', '1', '2', '1', '1512977784', '1513402562', 'fa-comment', '0', '5');
INSERT INTO `tplay_admin_menu` VALUES ('60', '修改短信配置', 'admin', 'smsconfig', 'publish', '', '修改短信配置。', '2', '1', '59', '1512977821', '1513402412', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('61', '发送测试短信', 'admin', 'smsconfig', 'smsto', '', '发送测试短信。', '2', '1', '59', '1512977851', '1513402421', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('62', '留言管理', 'admin', 'tomessages', 'index', '', '留言管理。', '1', '2', '35', '1513047149', '1513402094', 'fa-comments', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('63', '标记留言', 'admin', 'tomessages', 'mark', '', '标记留言。', '2', '1', '62', '1513047177', '1513402105', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('64', '删除留言', 'admin', 'tomessages', 'delete', '', '删除留言。', '2', '1', '62', '1513047205', '1513402113', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('65', '添加留言', 'admin', 'tomessages', 'publish', '', '添加留言。', '2', '2', '62', '1513047239', '1513402120', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('66', '管理员登录', 'admin', 'common', 'login', '', '管理员登录。', '2', '2', '0', '1513061455', '1513402429', '', '0', '100');
INSERT INTO `tplay_admin_menu` VALUES ('67', '网站配置', 'admin', 'webconfig', 'index', '', '网站信息设置。', '1', '2', '1', '1513131135', '1513408841', 'fa-desktop', '0', '3');
INSERT INTO `tplay_admin_menu` VALUES ('68', '修改网站配置', 'admin', 'webconfig', 'publish', '', '修改网站配置信息。', '2', '1', '67', '1513131161', '1513408856', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('69', '上传文件', 'admin', 'common', 'upload', '', '上传文件。', '2', '1', '0', '1513155130', '1513155130', null, '0', '200');
INSERT INTO `tplay_admin_menu` VALUES ('70', '上传附件', 'admin', 'attachment', 'upload', '', '上传附件。', '2', '1', '79', '1513323699', '1513403557', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('71', '文件下载', 'admin', 'attachment', 'download', '', '文件下载。', '2', '1', '79', '1513325699', '1513403571', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('72', '管理员', 'admin', 'admin', 'index', '', '管理员列表。', '1', '2', '4', '1513402959', '1513402959', 'fa-user', '0', '1');
INSERT INTO `tplay_admin_menu` VALUES ('76', '后台菜单', 'admin', 'menu', 'index', '', '添加/修改菜单。', '1', '2', '75', '1513403248', '1513403248', 'fa-sliders', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('80', '菜单排序', 'admin', 'menu', 'orders', '', '后台菜单排序。', '2', '1', '76', '1513408418', '1513408418', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('81', 'URL美化', 'admin', 'urlsconfig', 'index', '', 'URL美化设置。', '1', '2', '1', '1513574783', '1513574783', 'fa-link', '0', '6');
INSERT INTO `tplay_admin_menu` VALUES ('82', '新增/修改url美化', 'admin', 'urlsconfig', 'publish', '', '新增/修改url美化规则。', '2', '1', '81', '1513574935', '1513574935', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('83', '启用/禁用url美化', 'admin', 'urlsconfig', 'status', '', '启用/禁用url美化规则。', '2', '1', '81', '1513574979', '1513575215', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('84', '删除url美化规则', 'admin', 'urlsconfig', 'delete', '', '删除url美化规则。', '2', '1', '81', '1513575009', '1513575009', '', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('85', '药企管理', 'admin', 'store', 'index', '', '针对医药供货商的管理。', '1', '2', '0', '1514963476', '1514963617', 'fa-ambulance', '1', '0');
INSERT INTO `tplay_admin_menu` VALUES ('86', '供货企业列表', 'admin', 'store', 'lists', '', '医药供货商信息列表。', '1', '2', '85', '1514963808', '1514967752', 'fa-tasks', '1', '0');
INSERT INTO `tplay_admin_menu` VALUES ('87', '药店管理', 'admin', 'pharmacy', 'index', '', '入驻药店管理。', '1', '2', '0', '1514964135', '1514964135', 'fa-medkit', '1', '0');
INSERT INTO `tplay_admin_menu` VALUES ('88', '入驻药店列表', 'admin', 'pharmacy', 'lists', '', '入驻药店信息列表。', '1', '2', '87', '1514964215', '1515059469', 'fa-user-md', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('89', '供货企业添加', 'admin', 'store', 'add', '', '医药供货企业添加。', '1', '2', '85', '1514968013', '1514968424', 'fa-edit', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('90', '供货企业编辑', 'admin', 'store', 'edit', '', '医药供货企业编辑。', '2', '2', '85', '1514968094', '1514968094', 'fa-edit', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('91', '供货企业删除', 'admin', 'store', 'delete', '', '医药供货企业删除。', '2', '2', '85', '1514968243', '1514968243', 'fa-trash', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('92', '入驻药店添加', 'admin', 'pharmacy', 'add', '', '入驻药店添加。', '1', '2', '87', '1515059118', '1515059266', 'fa-edit', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('93', '入驻药店编辑', 'admin', 'pharmacy', 'edit', '', '入驻药店编辑。', '2', '2', '87', '1515059175', '1515059300', 'fa-edit', '0', '0');
INSERT INTO `tplay_admin_menu` VALUES ('94', '入驻药店删除', 'admin', 'pharmacy', 'delete', '', '入驻药店删除。', '2', '2', '87', '1515059237', '1515059310', 'fa-trash', '0', '0');

-- ----------------------------
-- Table structure for `tplay_article`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_article`;
CREATE TABLE `tplay_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `article_cate_id` int(11) NOT NULL,
  `thumb` int(11) DEFAULT NULL,
  `content` text,
  `admin_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `edit_admin_id` int(11) NOT NULL COMMENT '最后修改人',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0待审核1已审核',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tplay_article
-- ----------------------------

-- ----------------------------
-- Table structure for `tplay_article_cate`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_article_cate`;
CREATE TABLE `tplay_article_cate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `tag` varchar(250) DEFAULT NULL COMMENT '关键词',
  `description` varchar(250) DEFAULT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `pid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tplay_article_cate
-- ----------------------------

-- ----------------------------
-- Table structure for `tplay_attachment`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_attachment`;
CREATE TABLE `tplay_attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module` char(15) NOT NULL DEFAULT '' COMMENT '所属模块',
  `filename` char(50) NOT NULL DEFAULT '' COMMENT '文件名',
  `filepath` char(200) NOT NULL DEFAULT '' COMMENT '文件路径+文件名',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `fileext` char(10) NOT NULL DEFAULT '' COMMENT '文件后缀',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `uploadip` char(15) NOT NULL DEFAULT '' COMMENT '上传IP',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0未审核1已审核-1不通过',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  `admin_id` int(11) NOT NULL COMMENT '审核者id',
  `audit_time` int(11) NOT NULL COMMENT '审核时间',
  `use` varchar(200) DEFAULT NULL COMMENT '用处',
  `download` int(11) NOT NULL DEFAULT '0' COMMENT '下载量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='附件表';

-- ----------------------------
-- Records of tplay_attachment
-- ----------------------------
INSERT INTO `tplay_attachment` VALUES ('1', 'admin', '856fedb321ed602111ebdd50d30f83e8.jpeg', '\\uploads\\admin\\admin_thumb\\20171223\\856fedb321ed602111ebdd50d30f83e8.jpeg', '18140', 'jpeg', '1', '127.0.0.1', '1', '1514011508', '1', '1514011508', 'admin_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('2', 'admin', 'ea3993a14ec0fd0010c4dbcf40024a82.jpg', '\\uploads\\admin\\admin_thumb\\20180103\\ea3993a14ec0fd0010c4dbcf40024a82.jpg', '39098', 'jpg', '1', '127.0.0.1', '1', '1514964313', '1', '1514964313', 'admin_thumb', '0');
INSERT INTO `tplay_attachment` VALUES ('3', 'admin', '28d64a72f19b60360ae48e6e07f2e892.jpg', '\\uploads\\admin\\store_licence_picture\\20180104\\28d64a72f19b60360ae48e6e07f2e892.jpg', '39098', 'jpg', '1', '127.0.0.1', '1', '1515047495', '1', '1515047495', 'store_licence_picture', '0');
INSERT INTO `tplay_attachment` VALUES ('4', 'admin', '3d3b510d400f193f3842cd71050acb4e.jpg', '\\uploads\\admin\\store_licence_picture\\20180104\\3d3b510d400f193f3842cd71050acb4e.jpg', '39098', 'jpg', '1', '127.0.0.1', '1', '1515047640', '1', '1515047640', 'store_licence_picture', '0');
INSERT INTO `tplay_attachment` VALUES ('5', 'admin', 'e1c7655ff6a562318a899fff480d51bb.jpg', '\\uploads\\admin\\store_licence_picture\\20180104\\e1c7655ff6a562318a899fff480d51bb.jpg', '39098', 'jpg', '1', '127.0.0.1', '1', '1515048053', '1', '1515048053', 'store_licence_picture', '0');
INSERT INTO `tplay_attachment` VALUES ('6', 'admin', 'a2ec496206662aaf8176a779ead95f2a.jpg', '\\uploads\\admin\\store_licence_picture\\20180104\\a2ec496206662aaf8176a779ead95f2a.jpg', '39098', 'jpg', '1', '127.0.0.1', '1', '1515048670', '1', '1515048670', 'store_licence_picture', '0');
INSERT INTO `tplay_attachment` VALUES ('7', 'admin', '323ee8adec01d3a3cf2d0c88aaf731b9.jpg', '\\uploads\\admin\\store_licence_picture\\20180104\\323ee8adec01d3a3cf2d0c88aaf731b9.jpg', '39098', 'jpg', '1', '127.0.0.1', '1', '1515049155', '1', '1515049155', 'store_licence_picture', '0');
INSERT INTO `tplay_attachment` VALUES ('8', 'admin', 'ab63b393549ce33d89042fe24ba7082a.jpg', '\\uploads\\admin\\store_licence_picture\\20180104\\ab63b393549ce33d89042fe24ba7082a.jpg', '39098', 'jpg', '1', '127.0.0.1', '1', '1515049689', '1', '1515049689', 'store_licence_picture', '0');
INSERT INTO `tplay_attachment` VALUES ('9', 'admin', 'ef748e3a73b95e946f958fc11672dc2a.jpg', '\\uploads\\admin\\store_licence_picture\\20180104\\ef748e3a73b95e946f958fc11672dc2a.jpg', '5788', 'jpg', '1', '127.0.0.1', '1', '1515051722', '1', '1515051722', 'store_licence_picture', '0');
INSERT INTO `tplay_attachment` VALUES ('10', 'admin', 'ca212aa90f6ebb8af6bf5f19daf10c1c.png', '\\uploads\\admin\\pharmacy_licence_picture\\20180104\\ca212aa90f6ebb8af6bf5f19daf10c1c.png', '9898', 'png', '1', '127.0.0.1', '1', '1515060246', '1', '1515060246', 'pharmacy_licence_picture', '0');

-- ----------------------------
-- Table structure for `tplay_emailconfig`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_emailconfig`;
CREATE TABLE `tplay_emailconfig` (
  `email` varchar(5) NOT NULL COMMENT '邮箱配置标识',
  `from_email` varchar(50) NOT NULL COMMENT '邮件来源也就是邮件地址',
  `from_name` varchar(50) NOT NULL,
  `smtp` varchar(50) NOT NULL COMMENT '邮箱smtp服务器',
  `username` varchar(100) NOT NULL COMMENT '邮箱账号',
  `password` varchar(100) NOT NULL COMMENT '邮箱密码',
  `title` varchar(200) NOT NULL COMMENT '邮件标题',
  `content` text NOT NULL COMMENT '邮件模板'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tplay_emailconfig
-- ----------------------------
INSERT INTO `tplay_emailconfig` VALUES ('email', '', '', '', '', '', '', '');

-- ----------------------------
-- Table structure for `tplay_messages`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_messages`;
CREATE TABLE `tplay_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` int(11) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `is_look` int(1) NOT NULL DEFAULT '0' COMMENT '0未读1已读',
  `message` text NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tplay_messages
-- ----------------------------

-- ----------------------------
-- Table structure for `tplay_pharmacy`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_pharmacy`;
CREATE TABLE `tplay_pharmacy` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `pharmacy_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '药店名称',
  `pharmacy_CUFCC` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '统一社会信用代码',
  `pharmacy_organization_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '组织机构代码',
  `pharmacy_registration_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '注册号',
  `pharmacy_operating_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '经营状态',
  `pharmacy_industry` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '所属行业',
  `pharmacy_establishment_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '成立日期',
  `pharmacy_business_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '公司类型',
  `pharmacy_operation_period` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '营业期限',
  `pharmacy_legal_representative` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '法定代表人',
  `pharmacy_isuse_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '发照日期',
  `pharmacy_registered_capital` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '注册资本',
  `pharmacy_registration_authority` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '登记机关',
  `pharmacy_business_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '药店地址',
  `pharmacy_scope_business` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '经营范围',
  `pharmacy_contact_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '药店联系电话',
  `pharmacy_contact_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '药店邮箱',
  `pharmacy_licence_picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '营业执照图片',
  `pharmacy_account_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款人(银行开户名称)',
  `pharmacy_account_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款账号',
  `pharmacy_account_cash_bank` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款银行',
  `pharmacy_account_cash_bank_branch` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款支行名称',
  `pharmacy_create_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '申请时间',
  `pharmacy_auth_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '认证时间',
  `pharmacy_auth_status` int(11) DEFAULT '1' COMMENT '认证状态[1:认证中 2:认证通过 3:认证失败]',
  `pharmacy_siteurl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '药店官网',
  `pharmacy_intro` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '药店简介',
  `pharmacy_longitude` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '经度',
  `pharmacy_latitude` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '纬度',
  `pharmacy_province_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '省名称',
  `pharmacy_province_id` int(11) DEFAULT '0' COMMENT '省id',
  `pharmacy_city_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '市名称',
  `pharmacy_city_id` int(11) DEFAULT '0' COMMENT '市id',
  `pharmacy_district_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '区名称',
  `pharmacy_district_id` int(11) DEFAULT '0' COMMENT '区id',
  `pharmacy_road_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '街道名称',
  `pharmacy_road_id` int(11) DEFAULT '0' COMMENT '街道id',
  `remark` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  `status` int(11) DEFAULT '1' COMMENT '启用状态[1:启用 2:禁用]',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='药店信息';

-- ----------------------------
-- Records of tplay_pharmacy
-- ----------------------------
INSERT INTO `tplay_pharmacy` VALUES ('1', '西安怡康医药连锁有限责任公司', '9161010473507449XG', '73507449X', '610100100044932', '存续', '零售业', '2002-07-22', '有限责任公司', '2002年7月22日 - 2032年6月2日', '何煜', '2017-10-19', '7000万', '西安市工商行政管理局', '西安市大庆路副24号', '化学药制剂、中成药、生化药品、抗生素、中药材、中药饮片、生物制品（除疫苗）的零售；第一类、二类、三类医疗器械的零售；预包装食品、散装食品、婴幼儿配方乳粉、其他婴幼儿配方食品、保健食品、特殊医学用途配方食品的销售；中医科；推拿按摩；水电费、宽带费、电话费代缴；城市一卡通IC卡的发售、充值代缴；商业预付卡的代理及发售；计算机软硬件、照相器材、电讯器材、照明器材、孕妇婴幼儿用品、儿童玩具、服装鞋帽、消毒用品、计生用品、针纺织品、化妆品、日用百货、洗涤用品、健身器械、电子产品、家用电器、五金交电、皮革、塑料制品、办公文化用品、保健用品、农副产品、眼镜的销售；场地租赁；医药信息咨询服务、健康管理咨询服务。（依法须经批准的项目，经相关部门批准后方可开展经营活动）', '029-87513026', '', '10', '', '', '', '', '2018-01-04', null, '1', 'http://www.ykyyls.com/', '怡康医药集团成立于2001年，是以医药零售为核心的多元化集团企业，旗下专业子公司15家（医药批发、怡康璞太和中医馆、中药饮片厂、玖和便利连锁超市、怡康婴乐宝母婴用品连锁店等）；2004—2010年连续七年，西安怡康连锁位列“中国零售连锁药店三十强” 2005年被定为陕西省医药行业政府重点扶持企业，2006年获得“十一五”陕西最具活力民营企业奖。2009年1月在“2008中国西安一经济影响力”评选中，怡康医药荣获\"西安十大最具经济影响力品牌”，2009年1 2月，荣获“2009年度西安经济最具成长性企业”， 2010年11月，“怡康”被评为陕西省著名商标，2012年4月，“怡康”被评为中国连锁百强综合实力排名16名，西北区域第1名，2012年12月，被评为陕西区域最具影响力企业奖，2012年12月，被评为陕西最具推动力公益企业。目前零售门店500多家，员工超过5000人。因公司发展需求，面向社会诚聘有理想、有抱负，与企业共创辉煌的精英人才。应聘地址：西安市大庆路付24号（乘坐公交车611路、103路到终点站汉城路下车，十字路口往东走100米路北即到）联系电话：84618261 84622', null, null, null, '0', null, '0', null, '0', null, '0', '', '1');

-- ----------------------------
-- Table structure for `tplay_smsconfig`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_smsconfig`;
CREATE TABLE `tplay_smsconfig` (
  `sms` varchar(10) NOT NULL DEFAULT 'sms' COMMENT '标识',
  `appkey` varchar(200) NOT NULL,
  `secretkey` varchar(200) NOT NULL,
  `type` varchar(100) DEFAULT 'normal' COMMENT '短信类型',
  `name` varchar(100) NOT NULL COMMENT '短信签名',
  `code` varchar(100) NOT NULL COMMENT '短信模板ID',
  `content` text NOT NULL COMMENT '短信默认模板'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tplay_smsconfig
-- ----------------------------
INSERT INTO `tplay_smsconfig` VALUES ('sms', '', '', '', '', '', '');

-- ----------------------------
-- Table structure for `tplay_store`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_store`;
CREATE TABLE `tplay_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `store_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '供应商名称',
  `store_CUFCC` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '统一社会信用代码',
  `store_organization_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '组织机构代码',
  `store_registration_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '注册号',
  `store_operating_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '经营状态',
  `store_industry` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '所属行业',
  `store_establishment_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '成立日期',
  `store_business_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '公司类型',
  `store_operation_period` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '营业期限',
  `store_legal_representative` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '法定代表人',
  `store_isuse_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '发照日期',
  `store_registered_capital` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '注册资本',
  `store_registration_authority` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '登记机关',
  `store_business_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '企业地址',
  `store_scope_business` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '经营范围',
  `store_contact_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '企业联系电话',
  `store_contact_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '企业邮箱',
  `store_licence_picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '营业执照图片',
  `store_account_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款人(银行开户名称)',
  `store_account_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款账号',
  `store_account_cash_bank` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款银行',
  `store_account_cash_bank_branch` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收款支行名称',
  `store_create_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '申请时间',
  `store_auth_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '认证时间',
  `store_auth_status` int(11) DEFAULT '1' COMMENT '认证状态[1:认证中 2:认证通过 3:认证失败]',
  `store_siteurl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '企业官网',
  `store_intro` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '公司简介',
  `store_longitude` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '经度',
  `store_latitude` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '纬度',
  `store_province_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '省名称',
  `store_province_id` int(11) DEFAULT '0' COMMENT '省id',
  `store_city_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `store_city_id` int(11) DEFAULT '0' COMMENT '市id',
  `store_district_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '区名称',
  `store_district_id` int(11) DEFAULT '0' COMMENT '区id',
  `store_road_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '街道名称',
  `store_road_id` int(11) DEFAULT '0' COMMENT '街道id',
  `remark` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  `status` int(11) DEFAULT '1' COMMENT '启用状态[1:启用 2:禁用]',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='医药供货企业信息';

-- ----------------------------
-- Records of tplay_store
-- ----------------------------
INSERT INTO `tplay_store` VALUES ('1', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '9', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('2', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '9', '', '', '', '', '2018-01-04', null, '2', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '2');
INSERT INTO `tplay_store` VALUES ('3', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '9', '', '', '', '', '2018-01-04', null, '3', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('4', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '9', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('5', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '9', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('6', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '9', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('7', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '9', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('8', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '9', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('9', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '9', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('10', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '9', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('11', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '9', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('12', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '9', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('13', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '9', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('14', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '9', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('15', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '9', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('16', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '9', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('17', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '9', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('18', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '9', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('19', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '9', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');
INSERT INTO `tplay_store` VALUES ('20', '华润医药商业集团有限公司', '911100007226178547', '722617854', '110000001889557', '开业', '批发业', '2000-12-27', '其他有限责任公司', '2000年12月27日 - -', '李向明', '2017-03-13', '517164.225044万元人民币', '北京市工商行政管理局东城分局', '北京市东城区安定门内大街257号', '销售中药饮片、中成药、化学药制剂、抗生素、生化药品、化学原料药、生物制品、体外诊断试剂、麻醉药品和第一类精神药品（含小包装原料药、小包装麻黄素原料、罂粟壳）、第二类精神药品（含原料药）、医疗用毒性药品（西药品种不含A型肉毒毒素、中药饮片）、蛋白同化制剂和肽类激素、保健食品、营养补剂、医疗器械、计生用品、包装食品；货物包装托运（仅限分公司经营）；普通货物运输；冷藏保温运输；销售百货、化妆品、计算机软硬件、电子设备、五金交电、家用电器、制药机械设备、办公用品、办公设备、家具；医药科技信息咨询（不含中介服务）；自营和代理各类商品及技术的进出口业务，但国家限定公司经营或禁止进出口的商品及技术除外；仓储装卸服务；展览、展示；医院药库管理服务；会议服务。（企业依法自主选择经营项目，开展经营活动；依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动；不得从事本市产业政策禁止和限制类项目的经营活动。）', '010-64057801', 'hryy@crpharm.com', '9', '', '', '', '', '2018-01-04', null, '1', 'http://www.crpharm.com/', '华润医药集团有限公司是华润（集团）有限公司根据国务院国资委“打造央企医药平台”的要求，在重组央企华源集团、三九集团医药资源的基础上成立的大型药品制造和分销企业，为华润集团整合发展国内医药产业的全资企业。', null, null, null, '0', null, null, null, null, null, null, '', '1');

-- ----------------------------
-- Table structure for `tplay_urlconfig`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_urlconfig`;
CREATE TABLE `tplay_urlconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aliases` varchar(200) NOT NULL COMMENT '想要设置的别名',
  `url` varchar(200) NOT NULL COMMENT '原url结构',
  `desc` text COMMENT '备注',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '0禁用1使用',
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tplay_urlconfig
-- ----------------------------

-- ----------------------------
-- Table structure for `tplay_webconfig`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_webconfig`;
CREATE TABLE `tplay_webconfig` (
  `web` varchar(20) NOT NULL COMMENT '网站配置标识',
  `name` varchar(200) NOT NULL COMMENT '网站名称',
  `keywords` text COMMENT '关键词',
  `desc` text COMMENT '描述',
  `is_log` int(1) NOT NULL DEFAULT '1' COMMENT '1开启日志0关闭',
  `file_type` varchar(200) DEFAULT NULL COMMENT '允许上传的类型',
  `file_size` bigint(20) DEFAULT NULL COMMENT '允许上传的最大值',
  `statistics` text COMMENT '统计代码',
  `black_ip` text COMMENT 'ip黑名单',
  `url_suffix` varchar(20) DEFAULT NULL COMMENT 'url伪静态后缀'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tplay_webconfig
-- ----------------------------
INSERT INTO `tplay_webconfig` VALUES ('web', 'Eyao订货系统管理后台', 'Tplay,后台管理,thinkphp5,layui', 'Tplay是一款基于ThinkPHP5.0.12 + layui2.2.45 + ECharts + Mysql开发的后台管理框架，集成了一般应用所必须的基础性功能，为开发者节省大量的时间。', '1', 'jpg,png,gif,mp4,zip,jpeg', '500', '', '', null);
