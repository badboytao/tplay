/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50553
Source Host           : 127.0.0.1:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-01-16 16:07:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tplay_drug`
-- ----------------------------
DROP TABLE IF EXISTS `tplay_drug`;
CREATE TABLE `tplay_drug` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `drug_store_id` int(11) DEFAULT NULL COMMENT '所属药企id',
  `drug_store_house_id` int(11) DEFAULT 0 COMMENT '所属药企仓库id',
  `drug_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL COMMENT '药品名称',
  `drug_thumb` varchar(255) CHARACTER SET latin1 DEFAULT NULL COMMENT '药品图片',
  `drug_cate_id` int(11) DEFAULT NULL COMMENT '药品类型ID',
  `drug_cate_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL COMMENT '药品类型',
  `drug_price` decimal(11,2) DEFAULT '0.00' COMMENT '药品价格',
  `drug_factory` varchar(255) CHARACTER SET latin1 DEFAULT NULL COMMENT '药品厂家',
  `drug_ingredients` text CHARACTER SET latin1 COMMENT '药品成分',
  `drug_functions` text CHARACTER SET latin1 COMMENT '功能主治',
  `drug_dosage` varchar(500) CHARACTER SET latin1 DEFAULT NULL COMMENT '用法用量',
  `drug_contraindication` varchar(500) CHARACTER SET latin1 DEFAULT NULL COMMENT '禁忌症',
  `drug_insurance_type` varchar(255) CHARACTER SET latin1 DEFAULT NULL COMMENT '医保类型',
  `drug_attribute` varchar(255) CHARACTER SET latin1 DEFAULT NULL COMMENT '药品属性',
  `drug_considerations` varchar(500) CHARACTER SET latin1 DEFAULT NULL COMMENT '注意事项',
  `drug_prepared` varchar(255) CHARACTER SET latin1 DEFAULT NULL COMMENT '剂型',
  `drug_indication` varchar(500) CHARACTER SET latin1 DEFAULT NULL COMMENT '适应症',
  `drug_pharmacological` varchar(500) CHARACTER SET latin1 DEFAULT NULL COMMENT '药理作用',
  `drug_unreaction` varchar(500) CHARACTER SET latin1 DEFAULT NULL COMMENT '不良反应',
  `drug_interaction` varchar(500) CHARACTER SET latin1 DEFAULT NULL COMMENT '药物相互作用',
  `drug_storage` varchar(500) CHARACTER SET latin1 DEFAULT NULL COMMENT '贮藏',
  `drug_expiry_date` varchar(255) CHARACTER SET latin1 DEFAULT NULL COMMENT '有效期',
  `drug_license_number` varchar(500) CHARACTER SET latin1 DEFAULT NULL COMMENT '批准文号',
  `drug_manufacturing_enterprise` varchar(500) CHARACTER SET latin1 DEFAULT NULL COMMENT '生产企业',
  `status` int(1) DEFAULT '0' COMMENT '上架状态[0:仓库中 1:已上架 2:已下架]',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='药品数据表';

-- ----------------------------
-- Records of tplay_drug
-- ----------------------------
